!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!      This subroutine calculates the strain distribution              !
!              inside and around the quantum dot                       !
!                                                                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SUBROUTINE STRAIN0(EXX,EYY,EZZ,EXY,EXZ,EYZ)

              Use Input_Data
              Use Dot_Geometry
              Use Auxiliar_Procedures, ONLY : AISO
              Use STRAIN_CALC
      
      IMPLICIT NONE

!!!!! 'dummy' and local variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      REAL         ZM,THETA,CTHETA,STHETA,CHI, &
                   EESUM,EEDIF,EERR,EE00,EEZZ,E2ZZ,EERZ,&
                   EEXX,EEYY,EEXY,EEXZ,EEYZ,X,Y,Z,ZMAUX
      REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXY,EXZ,EYZ 
      INTEGER      I_X,I_Y,I_Z,I_N1,I_N2,I_N3

      REAL, DIMENSION(3) :: R_SL,X_VEC,XI_VEC

      !!! COMMON
      REAL :: RHO,ZETA,ETA
      COMMON /QAGON/RHO,ZETA,ETA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      EXX=0.E0;EYY=0.E0;EZZ=0.E0;EXY=0.E0;EXZ=0.E0;EYZ=0.E0

ZD:   DO I_Z=1,ZDim
        WRITE(16,'(A,I3,A,I3)')"I_Z ",I_Z," of ",ZDIM
        Z=Z_Min+REAL(I_Z-1)*Z_Inc
YD:   DO I_Y=1,YDim
        Y=Y_Min+REAL(I_Y-1)*Y_Inc
XD:   DO I_X=1,XDim
        X=X_Min+REAL(I_X-1)*X_Inc

        X_VEC=(/X,Y,Z/)

       EEXX=0.E0;EEYY=0.E0;EEXY=0.E0;EEXZ=0.E0;EEYZ=0.E0;EEZZ=0.E0

N3:   DO I_N3=NMin_Z,NMax_Z
N2:   DO I_N2=NMin_Y,NMax_Y
N1:   DO I_N1=NMin_X,NMax_X

      R_SL=REAL(I_N1)*A1_S+REAL(I_N2)*A2_S+REAL(I_N3)*A3_S

      XI_VEC=X_VEC-R_SL

      RHO=SQRT(XI_VEC(1)**2+XI_VEC(2)**2)/RC
      IF(XI_VEC(1).EQ.0.E0.AND.XI_VEC(2).EQ.0.E0) THEN
              CTHETA=1.E0/SQRT(2.E0); STHETA=1.E0/SQRT(2.E0) ! It is not the Mathematical limit
      ELSE
              THETA=ATAN(XI_VEC(2)/XI_VEC(1))
              CTHETA=Cos(THETA); STHETA=Sin(THETA)
      END IF
      ZETA=XI_VEC(3)/ZC

          IF (RHO.LE.RD) THEN
             CALL SHAPERTOZ(MIN(RHO*RC,Rqd_Base),ZMAUX)
             ZM=ZMAUX/ZC
          ELSE
             ZM = 0.E0
          END IF

    
          IF (abs(zeta) .EQ. 0.E0 .OR. ZETA .EQ. ZM) THEN 
              ZETA=ZETA-1.E-5
          END IF

          CHI = 0.
          IF (RHO.LE.RD.AND.ZETA.GE.0.E0.AND.ZETA.LE.ZM) THEN
                  CHI = 1.
                  if(I_N1.NE.0.OR.I_N2.NE.0.OR.I_N3.NE.0) THEN
                          WRITE(16,*)I_N1,I_N2,I_N3
                          WRITE(16,*)X_VEC(3),ZETA*ZC,ZM*ZC
                  END IF
          END IF

          IF (AISO.EQ.1) THEN
             CALL STRISO(EESUM,EEDIF,E2ZZ,EERZ,CHI)
          ELSE
             CALL STRANISO(EESUM,EEDIF,E2ZZ,EERZ,CHI)
          END IF
          
           IF (RHO .EQ. 0.E0) EEDIF = 0.E0
   
           EERR  = (EESUM+EEDIF)/2.
           EE00  = (EESUM-EEDIF)/2. 


!!!!!!!!!!!!!!!!!! Wetting Layer !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          IF(I_N1.EQ.0.AND.I_N2.EQ.0.AND.I_N3.EQ.0) THEN
           IF (ZETA.LT.0.E0 .AND. ZETA.GE.-D) THEN

               EESUM = EESUM + BISUM
               EERR  = EERR  + BISUM/2.E0
               EE00  = EE00  + BISUM/2.E0
               IF (AISO.EQ.1) THEN
                  E2ZZ = E2ZZ + BIZZ             
               ELSE
                  E2ZZ = E2ZZ  + (-2.E0*C13/C33*EPSA) !BIZZ
               END IF

           END IF
          END IF
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

           EEXX = EEXX + (CTHETA**2*EERR + STHETA**2*EE00)
           EEYY = EEYY + (STHETA**2*EERR + CTHETA**2*EE00)
           EEZZ = EEZZ + E2ZZ
           EEXY = EEXY + (CTHETA*STHETA*(EERR-EE00))
           EEXZ = EEXZ + (CTHETA*EERZ)
           EEYZ = EEYZ + (STHETA*EERZ)

      END DO N1
      END DO N2
      END DO N3

           EXX(I_X,I_Y,I_Z)= EEXX
           EYY(I_X,I_Y,I_Z)= EEYY
           EZZ(I_X,I_Y,I_Z)= EEZZ
           EXY(I_X,I_Y,I_Z)= EEXY
           EXZ(I_X,I_Y,I_Z)= EEXZ
           EYZ(I_X,I_Y,I_Z)= EEYZ
         
!           WRITE(26,'(10(E15.8,1X))')Z,EEXX,EEYY,EEZZ,EEXY,EEXZ,EEYZ
       
      END DO XD
      END DO YD
      END DO ZD
      
!     STOP

      RETURN
      END SUBROUTINE STRAIN0

      DOUBLE PRECISION FUNCTION I_FA00(X)
       Use STRAIN_CALC
       Use Dot_Geometry
       Use Input_Data, ONLY: RC,ZC
       IMPLICIT NONE
      
       DOUBLE PRECISION :: X
       !!! COMMON
       REAL :: RHO,ZETA,ETA,ZM
       COMMON /QAGON/RHO,ZETA,ETA

       CALL SHAPERTOZ(MIN(SNGL(X)*RC,Rqd_Base),ZM)
       I_FA00=X*FA00(DBLE(RHO),DBLE(ZETA),X,DBLE(ZM/ZC),DBLE(ETA))

       RETURN

      END FUNCTION I_FA00

      DOUBLE PRECISION FUNCTION I_FA10(X)
       Use STRAIN_CALC
       Use Dot_Geometry
       Use Input_Data, ONLY: RC,ZC
       IMPLICIT NONE
      
       DOUBLE PRECISION :: X
       !!! COMMON
       REAL :: RHO,ZETA,ETA,ZM
       COMMON /QAGON/RHO,ZETA,ETA

       CALL SHAPERTOZ(MIN(SNGL(X)*RC,Rqd_Base),ZM)
       I_FA10=X*FA10(DBLE(RHO),DBLE(ZETA),X,DBLE(ZM/ZC),DBLE(ETA))
       
       RETURN

      END FUNCTION I_FA10

      DOUBLE PRECISION FUNCTION I_FA20(X)
       Use STRAIN_CALC
       Use Dot_Geometry
       Use Input_Data, ONLY: RC,ZC
       IMPLICIT NONE
      
       DOUBLE PRECISION :: X
       !!! COMMON
       REAL :: RHO,ZETA,ETA,ZM
       COMMON /QAGON/RHO,ZETA,ETA

       CALL SHAPERTOZ(MIN(SNGL(X)*RC,Rqd_Base),ZM)
       I_FA20=X*FA20(DBLE(RHO),DBLE(ZETA),X,DBLE(ZM/ZC),DBLE(ETA))
       
       RETURN

      END FUNCTION I_FA20
       

