
include make.ifort

OBJECTS = dot_geo.o aux_proc.o input.o ncpack_str.o\
          ncpack_pzo.o ncpack_pot.o elipt.o pzo_mod.o\
          str_mod.o pzo_slat.o str_slat.o pot_slat.o\
          displ_mod.o displ_slat.o ncpack_displ.o poten.o 

EXEC=poten
EXEC_D=dsdump

poten:    $(OBJECTS)
	 $(FORC)  $(FFLAGS) $(EXEC) $(OBJECTS) $(LFLAGS) $(LNETCDF) \
	 $(LSLATEC) $(LSCSL)

dsdump:    dsdump.o
	 $(FORC)  $(FFLAGS) $(EXEC_D) dsdump.o $(LFLAGS) $(LNETCDF) 

poten.o:  poten.f90 dot_geo.f90 aux_proc.f90 input.f90 ncpack_str.f90\
          ncpack_pzo.f90 ncpack_pot.f90 ncpack_displ.f90
	 $(FORC) $(FFLAGC) $(INETCDF) poten.f90

input.o:  input.f90 dot_geo.f90 aux_proc.f90
	 $(FORC) $(FFLAGC) input.f90

dsdump.o:  dsdump.f90 
	 $(FORC) $(FFLAGC) dsdump.f90 $(INETCDF)

elipt.o:  elipt.f90
	 $(FORC) $(FFLAGC) elipt.f90

pzo_mod.o:  pzo_mod.f90 input.f90 elipt.f90 
	 $(FORC) $(FFLAGC) pzo_mod.f90

str_mod.o:  str_mod.f90 input.f90 elipt.f90 
	 $(FORC) $(FFLAGC) str_mod.f90

displ_mod.o:  displ_mod.f90 input.f90 elipt.f90 \
            str_mod.f90 pzo_mod.f90
	 $(FORC) $(FFLAGC) displ_mod.f90

ncpack_pot.o:  ncpack_pot.f90 dot_geo.f90
	 $(FORC) $(FFLAGC) ncpack_pot.f90 $(INETCDF)

ncpack_pzo.o:  ncpack_pzo.f90 dot_geo.f90
	 $(FORC) $(FFLAGC) ncpack_pzo.f90 $(INETCDF)

ncpack_str.o:  ncpack_str.f90 dot_geo.f90
	 $(FORC) $(FFLAGC) ncpack_str.f90 $(INETCDF)

ncpack_displ.o:  ncpack_displ.f90 dot_geo.f90
	 $(FORC) $(FFLAGC) ncpack_displ.f90 $(INETCDF)

pzo_slat.o:  pzo_slat.f90 input.f90 dot_geo.f90\
             aux_proc.f90 pzo_mod.f90
	 $(FORC) $(FFLAGC) pzo_slat.f90

str_slat.o:  str_slat.f90 input.f90 dot_geo.f90\
             aux_proc.f90 str_mod.f90 
	 $(FORC) $(FFLAGC) str_slat.f90

displ_slat.o:  displ_slat.f90 input.f90 dot_geo.f90\
             aux_proc.f90 displ_mod.f90 
	 $(FORC) $(FFLAGC) displ_slat.f90

pot_slat.o:  pot_slat.f90 input.f90 dot_geo.f90\
             aux_proc.f90
	 $(FORC) $(FFLAGC) pot_slat.f90

dot_geo.o:    dot_geo.f90
	 $(FORC) $(FFLAGC) dot_geo.f90

aux_proc.o:    aux_proc.f90
	 $(FORC) $(FFLAGC) aux_proc.f90 $(INETCDF)
clean:
	 rm -f *.o *.d *.mod work.pc
