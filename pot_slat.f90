
    SUBROUTINE POTENTIAL_WZ(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                            ESOUP,ESODW,ELAST,EXX,EYY,&
                            EZZ,EXY,EXZ,EYZ,&
                            POT)

              Use Input_Data
              Use Dot_Geometry
              Use Auxiliar_Procedures, ONLY : AISO
      
      IMPLICIT NONE

!!!!! 'dummy' and local variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      REAL,DIMENSION(:,:,:),OPTIONAL ::  EXX,EYY,EZZ,EXY,EXZ,EYZ,POT

      REAL,DIMENSION(:,:,:) ::  EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                ESOUP,ESODW,ELAST

      REAL         ZM,THETA,CTHETA,STHETA, &
                   X,Y,Z,ZMAUX,RHO,ZETA 

      INTEGER      I_X,I_Y,I_Z,I_N1,I_N2,I_N3,CHI

      REAL, DIMENSION(3) :: R_SL,X_VEC,XI_VEC

      REAL :: POTBE,POTBHH,POTBLH,POTBSO,POTBLS,&
              POTWE,POTWHH,POTWLH,POTWSO,POTWLS,&
              VBIEL,VBIHH,VBILH,VBISO,VBILS

      REAL,DIMENSION(0:1) :: POTE, POTHH, POTLH, POTSO, POTLS, &
                             PC1, PC2, PD1, PD2, PD3, PD4, PD5, PD6

      REAL, DIMENSION(1:8) :: ENERGY

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      CALL ENER_CONSTANTS_WZ( )

ZD:   DO I_Z=1,ZDim
!       WRITE(16,'(A,I3,A,I3)')"I_Z ",I_Z," of ",ZDIM
        Z=Z_Min+REAL(I_Z-1)*Z_Inc
YD:   DO I_Y=1,YDim
        Y=Y_Min+REAL(I_Y-1)*Y_Inc
XD:   DO I_X=1,XDim
        X=X_Min+REAL(I_X-1)*X_Inc

        X_VEC=(/X,Y,Z/)

        ENERGY=0.E0

      I_N1=0; I_N2=0; I_N3=0

      R_SL=REAL(I_N1)*A1_S+REAL(I_N2)*A2_S+REAL(I_N3)*A3_S

      XI_VEC=X_VEC-R_SL

      RHO=SQRT(XI_VEC(1)**2+XI_VEC(2)**2)/RC
      IF(XI_VEC(1).EQ.0.E0.AND.XI_VEC(2).EQ.0.E0) THEN
              CTHETA=1.E0/SQRT(2.E0); STHETA=1.E0/SQRT(2.E0) ! It is not the Mathematical limit
      ELSE
              THETA=ATAN(XI_VEC(2)/XI_VEC(1))
              CTHETA=Cos(THETA); STHETA=Sin(THETA)
      END IF
      ZETA=XI_VEC(3)/ZC

          IF (RHO.LE.RD) THEN
             CALL SHAPERTOZ(MIN(RHO*RC,Rqd_Base),ZMAUX)
             ZM=ZMAUX/ZC
          ELSE
             ZM = 0.E0
          END IF
    
          IF (abs(zeta) .EQ. 0.E0 .OR. ZETA .EQ. ZM) THEN 
              ZETA=ZETA-1.E-5
          END IF

          CHI = 0
          IF (ZETA.GE.-D.AND.ZETA.LE.ZM) THEN
                  CHI = 1
                  IF(I_N1.NE.0.OR.I_N2.NE.0.OR.I_N3.NE.0) THEN
                          WRITE(16,*)I_N1,I_N2,I_N3
                          WRITE(16,*)X_VEC(3),ZETA*ZC,ZM*ZC
                  END IF
          END IF

          IF (KPBASIS .EQ. 0) THEN ! Winkler basis
            IF (KCOOR.EQ.0) THEN
                CALL POT_CALC_CAR_WZ(CHI,ENERGY)
            ELSE
                CALL POT_CALC_CYL_WZ(CHI,ENERGY)
            END IF
          ELSE
            IF (KCOOR.EQ.0) THEN
                CALL POT_CALC_CAR_CH_WZ(CHI,ENERGY)
            ELSE
                CALL POT_CALC_CYL_CH_WZ(CHI,ENERGY)
            END IF
          END IF

           EEL(I_X,I_Y,I_Z)   = ENERGY(1)
           EHHUP(I_X,I_Y,I_Z) = ENERGY(2)
           ELHUP(I_X,I_Y,I_Z) = ENERGY(3)
           ELHDW(I_X,I_Y,I_Z) = ENERGY(4)
           EHHDW(I_X,I_Y,I_Z) = ENERGY(5)
           ESOUP(I_X,I_Y,I_Z) = ENERGY(6)
           ESODW(I_X,I_Y,I_Z) = ENERGY(7)
           ELAST(I_X,I_Y,I_Z) = ENERGY(8)
         
!           WRITE(26,'(10(E15.8,1X))')Z,ENERGY(1:8)
       
      END DO XD
      END DO YD
      END DO ZD

!     STOP

      RETURN

      CONTAINS
      
      SUBROUTINE ENER_CONSTANTS_WZ( )
      IMPLICIT NONE

      REAL, DIMENSION(3) :: EW,EB
      REAL :: EVW,EVB,ECW,ECB
      INTEGER, DIMENSION(1) :: AUX
      INTEGER :: NW,NB

         EVW=0.E0; EVB=0.E0
         
         EW(1)=EVW+DW1+DW2
         EW(2)=EVW+(DW1-DW2)/2.E0+SQRT(((DW1-DW2)/2.E0)**2+2.E0*DW3**2)
         EW(3)=EVW+(DW1-DW2)/2.E0-SQRT(((DW1-DW2)/2.E0)**2+2.E0*DW3**2)
         
         EB(1)=EVB+DB1+DB2
         EB(2)=EVB+(DB1-DB2)/2.E0+SQRT(((DB1-DB2)/2.E0)**2+2.E0*DB3**2)
         EB(3)=EVB+(DB1-DB2)/2.E0-SQRT(((DB1-DB2)/2.E0)**2+2.E0*DB3**2)
         
         AUX=MAXLOC(EW); NW=AUX(1)
         AUX=MAXLOC(EB); NB=AUX(1)
         
         EVB=0.E0
         EVW=VBO+EB(NB)-EW(NW)
         
         EW(1)=EVW+DW1+DW2
         EW(2)=EVW+(DW1-DW2)/2.E0+SQRT(((DW1-DW2)/2.E0)**2+2.E0*DW3**2)
         EW(3)=EVW+(DW1-DW2)/2.E0-SQRT(((DW1-DW2)/2.E0)**2+2.E0*DW3**2)
         
         EB(1)=EVB+DB1+DB2
         EB(2)=EVB+(DB1-DB2)/2.E0+SQRT(((DB1-DB2)/2.E0)**2+2.E0*DB3**2)
         EB(3)=EVB+(DB1-DB2)/2.E0-SQRT(((DB1-DB2)/2.E0)**2+2.E0*DB3**2)
         
         ECW=EGW+MAXVAL(EW)
         ECB=EGB+MAXVAL(EB)
         
         VWE=ECW; VBE=ECB
         VWH=EVW; VBH=EVB

! Definition of parameters for Barrier and Well

            
         POTWE  =  VWE
         POTWHH = (VWH+DW1+DW2)
         POTBE  =  VBE
         POTBHH = (VBH+DB1+DB2)

         IF (KPBASIS == 0) THEN
           POTWLH = (VWH+(DW1-DW2+4.E0*DW3)/3.E0)
           POTWSO = (VWH+2.E0*(DW1-DW2-2.E0*DW3)/3.E0)
           POTWLS = (DW1-DW2+DW3)
           POTBLH = (VBH+(DB1-DB2+4.E0*DB3)/3.E0)
           POTBSO = (VBH+2.E0*(DB1-DB2-2.E0*DB3)/3.E0)
           POTBLS = (DB1-DB2+DB3)
         ELSE
           POTWLH = (VWH+DW1-DW2)
           POTWLS = DW3
           POTWSO = VWH
           POTBLH = (VBH+DB1-DB2)
           POTBLS = DB3
           POTBSO = VBH
         END IF

!!!!! Output Potential Profiles !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      WRITE(6,*)"PERFILES DE POTENCIAL SIN DEFORMACION:"
      WRITE(6,*)"POTWE= ",POTWE," POTBE= ",POTBE
      WRITE(6,*)"POTWHH= ",POTWHH," POTBHH= ",POTBHH
      WRITE(6,*)"POTWLH= ",POTWLH," POTBLH= ",POTBLH
      WRITE(6,*)"POTWSO= ",POTWSO," POTBSO= ",POTBSO
      WRITE(6,*)"POTWLS= ",POTWLS," POTBLS= ",POTBLS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      
!!!!! Output Potential Profiles including strain effect !!!!!!!!!!!!!!!!!!!!!!!!

         IF(STR_Action.EQ.0) THEN
            IF (KPBASIS == 0) THEN
              VBIEL=           ( C2*BISUM + C1*BIZZ )
              VBIHH=      ( (D2+D4)*BISUM + (D1+D3)*BIZZ )
              VBILH=   ( (D2+D4/3.)*BISUM + (D1+D3/3.)*BIZZ )
              VBISO=( (D2+2.*D4/3.)*BISUM + (D1+2.*D3/3.)*BIZZ )
              VBILS=           ( D4*BISUM + D3*BIZZ )
            ELSE
              VBIEL=        ( C2*BISUM + C1*BIZZ )
              VBIHH=   ( (D2+D4)*BISUM + (D1+D3)*BIZZ )
              VBILH=   ( (D2+D4)*BISUM + (D1+D3)*BIZZ )
              VBISO=         (D2*BISUM + D1*BIZZ )
              VBILS= 0.E0
            END IF
         END IF


             
!             Potential edges including strain effect
         IF(STR_Action.EQ.0) THEN
          POTWE= (POTWE+VBIEL)
          POTWHH= (POTWHH+VBIHH)
          POTWLH= (POTWLH+VBILH)
          POTWSO= (POTWSO+VBISO)
          POTWLS= (POTWLS+VBILS)
          WRITE(6,*)"PERFILES DE POTENCIAL CON DEFORMACION:"
          WRITE(6,*)"POTWE= ",POTWE," POTBE= ",POTBE
          WRITE(6,*)"POTWHH= ",POTWHH," POTBHH= ",POTBHH
          WRITE(6,*)"POTWLH= ",POTWLH," POTBLH= ",POTBLH
          WRITE(6,*)"POTWSO= ",POTWSO," POTBSO= ",POTBSO
          WRITE(6,*)"POTWLS= ",POTWLS," POTBLS= ",POTBLS
         END IF
             
             POTE(0)  = POTBE  ; POTE(1)  = POTWE
             POTHH(0) = POTBHH ; POTHH(1) = POTWHH
             POTLH(0) = POTBLH ; POTLH(1) = POTWLH
             POTLS(0) = POTBLS ; POTLS(1) = POTWLS
             POTSO(0) = POTBSO ; POTSO(1) = POTWSO
         
!!!! Deformation potentials for barrier and well are equal
         
         PC1(0) = C1 ; PC1(1) = C1
         PC2(0) = C2 ; PC2(1) = C2
         PD1(0) = D1 ; PD1(1) = D1
         PD2(0) = D2 ; PD2(1) = D2
         PD3(0) = D3 ; PD3(1) = D3
         PD4(0) = D4 ; PD4(1) = D4
         PD5(0) = D5 ; PD5(1) = D5
         PD6(0) = D6 ; PD6(1) = D6

         RETURN
      
      END SUBROUTINE ENER_CONSTANTS_WZ

      SUBROUTINE POT_CALC_CAR_CH_WZ(CHI,ENERGY)
      IMPLICIT NONE

        REAL, DIMENSION(:) :: ENERGY
        INTEGER, PARAMETER :: DIMM=6
        COMPLEX, DIMENSION(1:DIMM,1:DIMM) :: HKANE
        REAL :: SXX,SYY,SZZ,SXY,SXZ,SYZ,PZO
        INTEGER :: I,J,CHI
!! DIAGONALIZATION VARIABLES

        COMPLEX, DIMENSION(1:(2*DIMM-1)) :: WORK
        REAL, DIMENSION(1:(3*DIMM-2)) :: RWORK
        REAL         W(1:DIMM), ROOT(DIMM), CARAC(DIMM)
        INTEGER      INFO, LWORK

        LWORK=2*DIMM-1
        
        IF(STR_Action.EQ.0) THEN
          SXX=0.E0; SYY=0.E0; SZZ=0.E0
          SXY=0.E0; SXZ=0.E0; SYZ=0.E0
        ELSE
          SXX=EXX(I_X,I_Y,I_Z)
          SYY=EYY(I_X,I_Y,I_Z)
          SZZ=EZZ(I_X,I_Y,I_Z)
          SXY=EXY(I_X,I_Y,I_Z)
          SXZ=EXZ(I_X,I_Y,I_Z)
          SYZ=EYZ(I_X,I_Y,I_Z)
        END IF
        IF(PZO_Action.EQ.0.E0) THEN
          PZO=0.E0
        ELSE
          PZO=POT(I_X,I_Y,I_Z)
        END IF

         HKANE(1,1)=POTHH(CHI)-PZO+ &
                     (PD1(CHI)+PD3(CHI))*SZZ+(PD2(CHI)+PD4(CHI))*(SXX+SYY)

         HKANE(2,2)=POTLH(CHI)-PZO+ &
                     (PD1(CHI)+PD3(CHI))*SZZ+(PD2(CHI)+PD4(CHI))*(SXX+SYY)

         HKANE(3,3)=POTSO(CHI)-PZO+ &
                     (PD1(CHI))*SZZ+(PD2(CHI))*(SXX+SYY)
         
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=HKANE(2,2)
         HKANE(6,6)=HKANE(3,3)

         IF(DIAG_Action.NE.0) THEN 
         
          HKANE(1,2)=-PD5(CHI)*CMPLX(SXX-SYY,-2.E0*SXY)
          HKANE(1,3)=-PD6(CHI)*CMPLX(SXZ,-SYZ)
          HKANE(1,4)=0.E0
          HKANE(1,5)=0.E0
          HKANE(1,6)=0.E0
 
          HKANE(2,3)=PD6(CHI)*CMPLX(SXZ,+SYZ)
          HKANE(2,4)=0.E0
          HKANE(2,5)=0.E0
          HKANE(2,6)= SQRT(2.E0)*POTLS(CHI)

          HKANE(3,4)= 0.E0
          HKANE(3,5)= SQRT(2.E0)*POTLS(CHI)
          HKANE(3,6)= 0.E0
 
          HKANE(4,5)=-PD5(CHI)*CMPLX(SXX-SYY,2.E0*SXY)
          HKANE(4,6)=HKANE(2,3)
          HKANE(5,6)=HKANE(1,3)



          DO I=1,DIMM
           DO J=I+1,DIMM
            HKANE(J,I)=CONJG(HKANE(I,J))
           END DO
          END DO
        
          CALL CHEEV('V','U',DIMM,HKANE,DIMM,W,WORK,LWORK,RWORK,INFO)

          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(2(A,F6.3,1X))')"RHO=",RC*RHO,"ZETA=",ZETA*ZC
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 

          DO I=1,3
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=HKANE(J,I)*HKANE(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(5)
           CARAC(5)=CARAC(2)
           CARAC(3)=CARAC(3)+CARAC(6)
           CARAC(6)=CARAC(3)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(3).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements are calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(3,3)
          ROOT(2)=HKANE(6,6) !Redundant

       
         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ENERGY(1) = POTE(CHI)-PZO+(PC1(CHI)*SZZ+PC2(CHI)*(SXX+SYY))

         ENERGY(2) = ROOT(4)
         ENERGY(3) = ROOT(3)
         ENERGY(4) = ROOT(3)
         ENERGY(5) = ROOT(4)

         ENERGY(6) = ROOT(1)
         ENERGY(7) = ROOT(1)
!        ENERGY(7) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*XLAMB*Tr(e)**2+XMU*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ENERGY(8) = C11*(SXX**2 + SYY**2)+C33*SZZ**2+ &
                      2.E0*(XLAMB*(SXX*SYY)+C13*(SXX+SYY)*SZZ+ &
                      (C11-XLAMB)*SXY**2+2.E0*XMU*(SXZ**2+SYZ**2))

          ENERGY(8) = ENERGY(8)/2.E0

          RETURN

      END SUBROUTINE POT_CALC_CAR_CH_WZ

      SUBROUTINE POT_CALC_CYL_CH_WZ(CHI,ENERGY)
      IMPLICIT NONE

        REAL, DIMENSION(:) :: ENERGY
        INTEGER, PARAMETER :: DIMM=6
        REAL, DIMENSION(1:DIMM,1:DIMM) :: HKANE
        REAL :: PZO
        REAL :: SSUM,SDIF,SZZ,SRZ,SRR,S00
        INTEGER :: I,J,CHI
!! DIAGONALIZATION VARIABLES

        REAL ::    W(1:DIMM), WORK(1:10*DIMM), AW(1:DIMM,1:DIMM), &
                   ROOT(DIMM), CARAC(DIMM), SLAMCH
        INTEGER :: IWORK(DIMM*5), IFAIL(DIMM), INFO, NUM

        IF(STR_Action.EQ.0) THEN
          SSUM=0.E0; SDIF=0.E0; SZZ=0.E0;
          SRZ=0.E0; SRR=0.E0; S00=0.E0
        ELSE

!!! FOR CYLINDRICAL COORDINATES WE CALCULATE ONLY THE X-Z PLANE,
!!! THAT MEANS: THETA=0
          
          SRR=EXX(I_X,I_Y,I_Z)
          S00=EYY(I_X,I_Y,I_Z)
          SSUM=EXX(I_X,I_Y,I_Z)+EYY(I_X,I_Y,I_Z)
          SDIF=EXX(I_X,I_Y,I_Z)-EYY(I_X,I_Y,I_Z)
          SZZ=EZZ(I_X,I_Y,I_Z)
          SRZ=EXZ(I_X,I_Y,I_Z)
        END IF
        IF(PZO_Action.EQ.0.E0) THEN
          PZO=0.E0
        ELSE
          PZO=POT(I_X,I_Y,I_Z)
        END IF

         HKANE(1,1)=POTHH(CHI)-PZO+ &
                     (PD1(CHI)+PD3(CHI))*SZZ+(PD2(CHI)+PD4(CHI))*SSUM 

         HKANE(2,2)=POTLH(CHI)-PZO+ &
                     (PD1(CHI)+PD3(CHI))*SZZ+(PD2(CHI)+PD4(CHI))*SSUM 

         HKANE(3,3)=POTSO(CHI)-PZO+ &
                     PD1(CHI)*SZZ+PD2(CHI)*SSUM 
         
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=HKANE(2,2)
         HKANE(6,6)=HKANE(3,3)

         IF(DIAG_Action.NE.0) THEN 
         
         
          HKANE(1,2)=-PD5(CHI)*SDIF
          HKANE(1,3)=-PD6(CHI)*SRZ
          HKANE(1,4)=0.E0
          HKANE(1,5)=0.E0
          HKANE(1,6)=0.E0
 
          HKANE(2,3)=PD6(CHI)*SRZ
          HKANE(2,4)=0.E0
          HKANE(2,5)=0.E0
          HKANE(2,6)= SQRT(2.E0)*POTLS(CHI)

          HKANE(3,4)= 0.E0
          HKANE(3,5)= SQRT(2.E0)*POTLS(CHI)
          HKANE(3,6)= 0.E0
 
          HKANE(4,5)=-PD5(CHI)*SDIF
          HKANE(4,6)=HKANE(2,3)
          HKANE(5,6)=HKANE(1,3)

          DO I=1,6
           DO J=I+1,6
            HKANE(J,I)=HKANE(I,J)
           END DO
          END DO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Analytic solutions for Rho = 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!         ZETA = ZMIN+ZINC*IZ ! XZ normalized to ZC
!
!        W(1)=(HKANE(2,2)+HKANE(5,5))+
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        W(2)=(HKANE(2,2)+HKANE(5,5))-
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        write(24,'(4(f18.8,1x))')ZETA,HKANE(1,1),W(1)/2.,W(2)/2.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         
          CALL DSYEVX('V','A','U',DIMM,HKANE,DIMM,0.,0.,2,2, &
           2*SLAMCH('S'),NUM,W,AW,DIMM,WORK,10*DIMM,IWORK,IFAIL,INFO)
         
          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(A,10(I2,1X))')"IFAIL=",IFAIL(1:DIMM)
               write(6,'(2(A,F6.3,1X))')"RHO=",RC*RHO,"ZETA=",ZETA*ZC
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 
          WRITE(6,*)W(1:6)

          DO I=1,6
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=AW(J,I)*AW(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(5)
           CARAC(5)=CARAC(2)
           CARAC(3)=CARAC(3)+CARAC(6)
           CARAC(6)=CARAC(3)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(3).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements are calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(3,3)
          ROOT(2)=HKANE(6,6) !Redundant

         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ENERGY(1) = POTE(CHI)-PZO+(PC1(CHI)*SZZ+PC2(CHI)*SSUM)
                  
         ENERGY(2) = ROOT(4)
         ENERGY(3) = ROOT(3)
         ENERGY(4) = ROOT(3)
         ENERGY(5) = ROOT(4)
                  
         ENERGY(6) = ROOT(1)
         ENERGY(7) = ROOT(1)
!        ESODW(IR,IZ,IGEO) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*XLAMB*Tr(e)**2+XMU*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


          ENERGY(8) = C11*(SRR**2 + S00**2)+C33*SZZ**2+ &
                      2.E0*(XLAMB*(SRR*S00)+C13*(SRR+S00)*SZZ+ &
                      +4.E0*XMU*(SRZ**2))

          ENERGY(8) = ENERGY(8)/2.E0

          RETURN

      END SUBROUTINE POT_CALC_CYL_CH_WZ

      SUBROUTINE POT_CALC_CAR_WZ(CHI,ENERGY)
      IMPLICIT NONE

        REAL, DIMENSION(:) :: ENERGY
        INTEGER, PARAMETER :: DIMM=6
        COMPLEX, DIMENSION(1:DIMM,1:DIMM) :: HKANE
        REAL :: SXX,SYY,SZZ,SXY,SXZ,SYZ,PZO
        INTEGER :: I,J,CHI
!! DIAGONALIZATION VARIABLES

        COMPLEX, DIMENSION(1:(2*DIMM-1)) :: WORK
        REAL, DIMENSION(1:(3*DIMM-2)) :: RWORK
        REAL         W(1:DIMM), ROOT(DIMM), CARAC(DIMM)
        INTEGER      INFO, LWORK

        LWORK=2*DIMM-1
        
        IF(STR_Action.EQ.0) THEN
          SXX=0.E0; SYY=0.E0; SZZ=0.E0
          SXY=0.E0; SXZ=0.E0; SYZ=0.E0
        ELSE
          SXX=EXX(I_X,I_Y,I_Z)
          SYY=EYY(I_X,I_Y,I_Z)
          SZZ=EZZ(I_X,I_Y,I_Z)
          SXY=EXY(I_X,I_Y,I_Z)
          SXZ=EXZ(I_X,I_Y,I_Z)
          SYZ=EYZ(I_X,I_Y,I_Z)
        END IF
        IF(PZO_Action.EQ.0.E0) THEN
          PZO=0.E0
        ELSE
          PZO=POT(I_X,I_Y,I_Z)
        END IF

         HKANE(1,1)=POTHH(CHI)-PZO+ &
                     (PD1(CHI)+PD3(CHI))*SZZ+(PD2(CHI)+PD4(CHI))*(SXX+SYY)
         
         HKANE(2,2)=POTLH(CHI)-PZO+(PD1(CHI)+PD3(CHI)/3.E0)*SZZ+ &
                     (PD2(CHI)+PD4(CHI)/3.E0)*(SXX+SYY)
         HKANE(3,3)=HKANE(2,2)
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=POTSO(CHI)-PZO+(PD1(CHI)+2.E0*PD3(CHI)/3.E0)*SZZ+ &
                     (PD2(CHI)+2.E0*PD4(CHI)/3.E0)*(SXX+SYY)
         HKANE(6,6)=HKANE(5,5)

         IF(DIAG_Action.NE.0) THEN 
         
          HKANE(1,2)=-SQRT(2.E0/3.E0)*PD6(CHI)*CMPLX(SXZ,-SYZ)
          HKANE(1,3)=-1.E0/SQRT(3.E0)*PD5(CHI)*CMPLX(SXX-SYY,-2.E0*SXY)
          HKANE(1,4)=0.E0
          HKANE(1,5)=1.E0/SQRT(3.E0)*PD6(CHI)*CMPLX(SXZ,-SYZ)
          HKANE(1,6)=SQRT(2./3.)*PD5(CHI)*CMPLX(SXX-SYY,-2.E0*SXY)
 
          HKANE(2,3)=0.
          HKANE(2,4)=HKANE(1,3)
          HKANE(2,5)=SQRT(2.E0)/3.E0*( POTLS(CHI)+&
                                       (PD3(CHI)*SZZ+PD4(CHI)*(SXX+SYY)) )
          HKANE(2,6)=-PD6(CHI)*CMPLX(SXZ,-SYZ)

          HKANE(3,4)=-HKANE(1,2)
          HKANE(3,5)=-PD6(CHI)*CMPLX(SXZ,SYZ)
          HKANE(3,6)=-HKANE(2,5)
 
          HKANE(4,5)=-SQRT(2./3.)*PD5(CHI)*CMPLX(SXX-SYY,+2.E0*SXY)
          HKANE(4,6)=PD6(CHI)*CMPLX(SXZ,SYZ)/SQRT(3.E0)

          HKANE(5,6)=0



          DO I=1,DIMM
           DO J=I+1,DIMM
            HKANE(J,I)=CONJG(HKANE(I,J))
           END DO
          END DO
        
          CALL CHEEV('V','U',DIMM,HKANE,DIMM,W,WORK,LWORK,RWORK,INFO)

          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(2(A,F6.3,1X))')"RHO=",RC*RHO,"ZETA=",ZETA*ZC
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 

          DO I=1,6,2
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=HKANE(J,I)*HKANE(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(3)
           CARAC(3)=CARAC(2)
           CARAC(5)=CARAC(5)+CARAC(6)
           CARAC(6)=CARAC(5)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(5).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements were calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(5,5)
          ROOT(2)=HKANE(6,6) !Redundant

       
         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ENERGY(1) = POTE(CHI)-PZO+(PC1(CHI)*SZZ+PC2(CHI)*(SXX+SYY))

         ENERGY(2) = ROOT(4)
         ENERGY(3) = ROOT(3)
         ENERGY(4) = ROOT(3)
         ENERGY(5) = ROOT(4)

         ENERGY(6) = ROOT(1)
         ENERGY(7) = ROOT(1)
!        ENERGY(7) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*XLAMB*Tr(e)**2+XMU*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ENERGY(8) = C11*(SXX**2 + SYY**2)+C33*SZZ**2+ &
                      2.E0*(XLAMB*(SXX*SYY)+C13*(SXX+SYY)*SZZ+ &
                      (C11-XLAMB)*SXY**2+2.E0*XMU*(SXZ**2+SYZ**2))

          ENERGY(8) = ENERGY(8)/2.E0

          RETURN

      END SUBROUTINE POT_CALC_CAR_WZ

      SUBROUTINE POT_CALC_CYL_WZ(CHI,ENERGY)
      IMPLICIT NONE

        REAL, DIMENSION(:) :: ENERGY
        INTEGER, PARAMETER :: DIMM=6
        REAL, DIMENSION(1:DIMM,1:DIMM) :: HKANE
        REAL :: PZO
        REAL :: SSUM,SDIF,SZZ,SRZ,SRR,S00
        INTEGER :: I,J,CHI
!! DIAGONALIZATION VARIABLES

        REAL ::    W(1:DIMM), WORK(1:10*DIMM), AW(1:DIMM,1:DIMM), &
                   ROOT(DIMM), CARAC(DIMM), SLAMCH
        INTEGER :: IWORK(DIMM*5), IFAIL(DIMM), INFO, NUM

        IF(STR_Action.EQ.0) THEN
          SSUM=0.E0; SDIF=0.E0; SZZ=0.E0;
          SRZ=0.E0; SRR=0.E0; S00=0.E0
        ELSE

!!! FOR CYLINDRICAL COORDINATES WE CALCULATE ONLY THE X-Z PLANE,
!!! THAT MEANS: THETA=0
          
          SRR=EXX(I_X,I_Y,I_Z)
          S00=EYY(I_X,I_Y,I_Z)
          SSUM=EXX(I_X,I_Y,I_Z)+EYY(I_X,I_Y,I_Z)
          SDIF=EXX(I_X,I_Y,I_Z)-EYY(I_X,I_Y,I_Z)
          SZZ=EZZ(I_X,I_Y,I_Z)
          SRZ=EXZ(I_X,I_Y,I_Z)
        END IF
        IF(PZO_Action.EQ.0.E0) THEN
          PZO=0.E0
        ELSE
          PZO=POT(I_X,I_Y,I_Z)
        END IF

         HKANE(1,1)=POTHH(CHI)-PZO+ &
                     (PD1(CHI)+PD3(CHI))*SZZ+(PD2(CHI)+PD4(CHI))*SSUM 
         
         HKANE(2,2)=POTLH(CHI)-PZO+(PD1(CHI)+PD3(CHI)/3.E0)*SZZ+ &
                     (PD2(CHI)+PD4(CHI)/3.E0)*SSUM
         HKANE(3,3)=HKANE(2,2)
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=POTSO(CHI)-PZO+(PD1(CHI)+2.E0*PD3(CHI)/3.E0)*SZZ+ &
                     (PD2(CHI)+2.E0*PD4(CHI)/3.E0)*SSUM
         HKANE(6,6)=HKANE(5,5)

         IF(DIAG_Action.NE.0) THEN 
         
          HKANE(1,2)=-SQRT(2.E0/3.E0)*PD6(CHI)*SRZ
          HKANE(1,3)=-1.E0/SQRT(3.E0)*PD5(CHI)*SDIF
          HKANE(1,4)=0.
          HKANE(1,5)=1.E0/SQRT(3.E0)*PD6(CHI)*SRZ
          HKANE(1,6)=SQRT(2./3.)*PD5(CHI)*SDIF
 
          HKANE(2,3)=0.
          HKANE(2,4)=HKANE(1,3)
          HKANE(2,5)=SQRT(2.E0)/3.E0*(POTLS(CHI)+(PD3(CHI)*SZZ+PD4(CHI)*SSUM))
          HKANE(2,6)=-PD6(CHI)*SRZ

          HKANE(3,4)=-HKANE(1,2)
          HKANE(3,5)=HKANE(2,6)
          HKANE(3,6)=-HKANE(2,5)
 
          HKANE(4,5)=-HKANE(1,6)
          HKANE(4,6)=HKANE(1,5)

          HKANE(5,6)=0



          DO I=1,6
           DO J=I+1,6
            HKANE(J,I)=HKANE(I,J)
           END DO
          END DO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Analytic solutions for Rho = 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!         ZETA = ZMIN+ZINC*IZ ! XZ normalized to ZC
!
!        W(1)=(HKANE(2,2)+HKANE(5,5))+
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        W(2)=(HKANE(2,2)+HKANE(5,5))-
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        write(24,'(4(f18.8,1x))')ZETA,HKANE(1,1),W(1)/2.,W(2)/2.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         
          CALL DSYEVX('V','A','U',DIMM,HKANE,DIMM,0.,0.,2,2, &
           2*SLAMCH('S'),NUM,W,AW,DIMM,WORK,10*DIMM,IWORK,IFAIL,INFO)
         
          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(A,10(I2,1X))')"IFAIL=",IFAIL(1:DIMM)
               write(6,'(2(A,F6.3,1X))')"RHO=",RC*RHO,"ZETA=",ZETA*ZC
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 

          DO I=1,6,2
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=AW(J,I)*AW(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(3)
           CARAC(3)=CARAC(2)
           CARAC(5)=CARAC(5)+CARAC(6)
           CARAC(6)=CARAC(5)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(5).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements were calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(5,5)
          ROOT(2)=HKANE(6,6) !Redundant

       
         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ENERGY(1) = POTE(CHI)-PZO+(PC1(CHI)*SZZ+PC2(CHI)*SSUM)
                  
         ENERGY(2) = ROOT(4)
         ENERGY(3) = ROOT(3)
         ENERGY(4) = ROOT(3)
         ENERGY(5) = ROOT(4)
                  
         ENERGY(6) = ROOT(1)
         ENERGY(7) = ROOT(1)
!        ESODW(IR,IZ,IGEO) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*XLAMB*Tr(e)**2+XMU*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


          ENERGY(8) = C11*(SRR**2 + S00**2)+C33*SZZ**2+ &
                      2.E0*(XLAMB*(SRR*S00)+C13*(SRR+S00)*SZZ+ &
                      +4.E0*XMU*(SRZ**2))

          ENERGY(8) = ENERGY(8)/2.E0

          RETURN

      END SUBROUTINE POT_CALC_CYL_WZ

    END SUBROUTINE POTENTIAL_WZ

    SUBROUTINE POTENTIAL_ZB(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                            ESOUP,ESODW,ELAST,EXX,EYY,&
                            EZZ,EXY,EXZ,EYZ) 
                            

              Use Input_Data
              Use Dot_Geometry
              Use Auxiliar_Procedures, ONLY : AISO
      
      IMPLICIT NONE

!!!!! 'dummy' and local variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      REAL,DIMENSION(:,:,:),OPTIONAL ::  EXX,EYY,EZZ,EXY,EXZ,EYZ

      REAL,DIMENSION(:,:,:) ::  EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                ESOUP,ESODW,ELAST

      REAL         ZM,THETA,CTHETA,STHETA, &
                   X,Y,Z,ZMAUX,RHO,ZETA 

      INTEGER      I_X,I_Y,I_Z,I_N1,I_N2,I_N3,CHI

      REAL, DIMENSION(3) :: R_SL,X_VEC,XI_VEC

      REAL :: POTWE,POTWHH,POTWLH,POTWSO,&
              VBIEL,VBIHH,VBILH,VBISO

      REAL,DIMENSION(0:1) :: POTE, POTHH, POTLH, POTSO, &
                             DVD,DSD,DVU,DVSU,D2VU,D2VSU,DSO,ZBC1

      REAL, DIMENSION(1:8) :: ENERGY

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      CALL ENER_CONSTANTS_ZB( )

ZD:   DO I_Z=1,ZDim
!       WRITE(16,'(A,I3,A,I3)')"I_Z ",I_Z," of ",ZDIM
        Z=Z_Min+REAL(I_Z-1)*Z_Inc
YD:   DO I_Y=1,YDim
        Y=Y_Min+REAL(I_Y-1)*Y_Inc
XD:   DO I_X=1,XDim
        X=X_Min+REAL(I_X-1)*X_Inc

        X_VEC=(/X,Y,Z/)

      I_N1=0; I_N2=0; I_N3=0

      R_SL=REAL(I_N1)*A1_S+REAL(I_N2)*A2_S+REAL(I_N3)*A3_S

      XI_VEC=X_VEC-R_SL

      RHO=SQRT(XI_VEC(1)**2+XI_VEC(2)**2)/RC
      IF(XI_VEC(1).EQ.0.E0.AND.XI_VEC(2).EQ.0.E0) THEN
              CTHETA=1.E0/SQRT(2.E0); STHETA=1.E0/SQRT(2.E0) ! It is not the Mathematical limit
      ELSE
              THETA=ATAN(XI_VEC(2)/XI_VEC(1))
              CTHETA=Cos(THETA); STHETA=Sin(THETA)
      END IF
      ZETA=XI_VEC(3)/ZC

          IF (RHO.LE.RD) THEN
             CALL SHAPERTOZ(MIN(RHO*RC,Rqd_Base),ZMAUX)
             ZM=ZMAUX/ZC
          ELSE
             ZM = 0.E0
          END IF
    
          IF (abs(zeta) .EQ. 0.E0 .OR. ZETA .EQ. ZM) THEN 
              ZETA=ZETA-1.E-5
          END IF

          CHI = 0
          IF (ZETA.GE.-D.AND.ZETA.LE.ZM) THEN
                  CHI = 1
                  IF(I_N1.NE.0.OR.I_N2.NE.0.OR.I_N3.NE.0) THEN
                          WRITE(16,*)I_N1,I_N2,I_N3
                          WRITE(16,*)X_VEC(3),ZETA*ZC,ZM*ZC
                  END IF
          END IF

          IF (KCOOR.EQ.0) THEN
              CALL POT_CALC_CAR_ZB(CHI,ENERGY)
          ELSE
              CALL POT_CALC_CYL_ZB(CHI,ENERGY)
          END IF

           EEL(I_X,I_Y,I_Z)   = ENERGY(1)
           EHHUP(I_X,I_Y,I_Z) = ENERGY(2)
           ELHUP(I_X,I_Y,I_Z) = ENERGY(3)
           ELHDW(I_X,I_Y,I_Z) = ENERGY(4)
           EHHDW(I_X,I_Y,I_Z) = ENERGY(5)
           ESOUP(I_X,I_Y,I_Z) = ENERGY(6)
           ESODW(I_X,I_Y,I_Z) = ENERGY(7)
           ELAST(I_X,I_Y,I_Z) = ENERGY(8)
         
!           WRITE(26,'(10(E15.8,1X))')Z,ENERGY(1:8)
       
      END DO XD
      END DO YD
      END DO ZD

!     STOP

      RETURN

      CONTAINS
      
      SUBROUTINE ENER_CONSTANTS_ZB( )
      IMPLICIT NONE

! Definition of parameters for Barrier and Well

! If we set both terms equal to zero the spin-interaction in the
! deformation potentials is removed. The values of AVB,BB,DB are
! taken from C. Pryor, PRB, 57, 7190 (1998)

!        DSO(0) = VBH - VBSO ; DSO(1) = VWH - VWSO 
         DSO(0) = 0.0; DSO(1) = 0.0
     
         ZBC1(0) = -9.3 ; ZBC1(1) = ACW
         
         DVD(0) = 0.7E0 - 2./9. * DSO(0) 
         DVD(1) = AVW - 2./9. * DSO(1)
         DSD(0) = 0.7E0 + 4./9. * DSO(0) 
         DSD(1) = AVW + 4./9. * DSO(1)
         DVU(0) = -3./2. * (-2.0) + 1./3. * DSO(0) 
         DVU(1) = -3./2. * BW + 1./3. * DSO(1)
         DVSU(0) = -3./2. * (-2.0) - 1./6. * DSO(0) 
         DVSU(1) = -3./2. * BW - 1./6. * DSO(1)
         D2VU(0) = -SQRT(3.)/2. * (-5.4) + 1./3. * DSO(0) 
         D2VU(1) = -SQRT(3.)/2. * DW + 1./3. * DSO(1)
         D2VSU(0) = -SQRT(3.)/2. * (-5.4) - 1./6. * DSO(0) 
         D2VSU(1) = -SQRT(3.)/2. * DW - 1./6. * DSO(1)

!!! In the calculation the deformation potentials are equal across the structure
         DVD(0)=DVD(1); DSD(0)=DSD(1); DVU(0)=DVU(1)
         DVSU(0)=DVSU(1); D2VU(0)=D2VU(1); D2VSU(0)=D2VSU(1)
          
         DSO(0) = VBH - VBSO ; DSO(1) = VWH - VWSO 
         POTWE= VWE
         POTWHH=VWH
         POTWLH=VWH
         POTWSO=VWSO

          IF(STR_Action.EQ.0) THEN
           VBIEL  = ACW*(BISUM+BIZZ)
           VBIHH  = AVW*(BISUM+BIZZ)-(-3.E0*BW/2.E0)/3.E0*(BISUM-2.E0*BIZZ)
           VBILH  = AVW*(BISUM+BIZZ)+(-3.E0*BW/2.E0)/3.E0*(BISUM-2.E0*BIZZ)
           VBISO  = AVW*(BISUM+BIZZ)
          
!          Potential edges including strain effect
           POTWE= (VWE+VBIEL)
           POTWHH= (VWH+VBIHH)
           POTWLH= (VWH+VBILH)
           POTWSO= (VWSO+VBISO)
          END IF

         POTE(0)  = VBE  ; POTE(1)  = POTWE
         POTHH(0) = VBH ;  POTHH(1) = POTWHH
         POTLH(0) = VBH ;  POTLH(1) = POTWLH
         POTSO(0) = VBSO ; POTSO(1) = POTWSO
         
         RETURN
      
      END SUBROUTINE ENER_CONSTANTS_ZB

      SUBROUTINE POT_CALC_CAR_ZB(CHI,ENERGY)
      IMPLICIT NONE

        REAL, DIMENSION(:) :: ENERGY
        INTEGER, PARAMETER :: DIMM=6
        COMPLEX, DIMENSION(1:DIMM,1:DIMM) :: HKANE
        REAL :: SXX,SYY,SZZ,SXY,SXZ,SYZ,SHID,SDIF,STIL
        INTEGER :: I,J,CHI
!! DIAGONALIZATION VARIABLES

        COMPLEX, DIMENSION(1:(2*DIMM-1)) :: WORK
        REAL, DIMENSION(1:(3*DIMM-2)) :: RWORK
        REAL         W(1:DIMM), ROOT(DIMM), CARAC(DIMM)
        INTEGER      INFO, LWORK

        
        IF(STR_Action.EQ.0) THEN
          SXX=0.E0; SYY=0.E0; SZZ=0.E0
          SXY=0.E0; SXZ=0.E0; SYZ=0.E0
        ELSE
          SXX=EXX(I_X,I_Y,I_Z)
          SYY=EYY(I_X,I_Y,I_Z)
          SZZ=EZZ(I_X,I_Y,I_Z)
          SXY=EXY(I_X,I_Y,I_Z)
          SXZ=EXZ(I_X,I_Y,I_Z)
          SYZ=EYZ(I_X,I_Y,I_Z)
          SHID=SXX+SYY+SZZ
          SDIF=SXX-SYY
          STIL=SXX+SYY-2.E0*SZZ
        END IF

         HKANE(1,1)=POTHH(CHI)+DVD(CHI)*SHID &
                     -1./3.*DVU(CHI)*STIL
         HKANE(2,2)=POTLH(CHI)+DVD(CHI)*SHID &
                     +1./3.*DVU(CHI)*STIL
         HKANE(3,3)=HKANE(2,2)
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=POTSO(CHI)+DSD(CHI)*SHID

         HKANE(6,6)=HKANE(5,5)

         IF(DIAG_Action.NE.0.AND.STR_Action.NE.0) THEN 

           HKANE(1,2)=2./SQRT(3.)*D2VU(CHI)*CMPLX(SXZ,-SYZ)
           HKANE(1,3)=1./SQRT(3.)*CMPLX(DVU(CHI)*SDIF,-2.E0*D2VU(CHI)*SXY)
           HKANE(1,4)=0.E0
           HKANE(1,5)=-SQRT(2./3.)*D2VSU(CHI)*CMPLX(SXZ,-SYZ)
           HKANE(1,6)=-SQRT(2./3.)*CMPLX(DVU(CHI)*SDIF,-2.E0*D2VSU(CHI)*SXY)
  
           HKANE(2,3)=0.
           HKANE(2,4)=HKANE(1,3)
           HKANE(2,5)=-SQRT(2.)/3.*DVSU(CHI)*STIL
           HKANE(2,6)=SQRT(2.)*D2VSU(CHI)*CMPLX(SXZ,-SYZ)
  
           HKANE(3,4)=-HKANE(1,2)
           HKANE(3,5)=SQRT(2.)*D2VSU(CHI)*CMPLX(SXZ,SYZ)
           HKANE(3,6)=-HKANE(2,5)
  
           HKANE(4,5)=SQRT(2./3.)*CMPLX(DVU(CHI)*SDIF,2.E0*D2VSU(CHI)*SXY)
           HKANE(4,6)=-HKANE(3,5)/SQRT(3.E0)
  
           HKANE(5,6)=0

         DO I=1,6
          DO J=I+1,6
           HKANE(J,I)=CONJG(HKANE(I,J))
          END DO
         END DO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Analytic solutions for Rho = 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!         ZETA = ZMIN+ZINC*IZ ! XZ normalized to ZC
!
!        W(1)=(HKANE(2,2)+HKANE(5,5))+
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        W(2)=(HKANE(2,2)+HKANE(5,5))-
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        write(24,'(4(f18.8,1x))')ZETA,HKANE(1,1),W(1)/2.,W(2)/2.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          CALL CHEEV('V','U',DIMM,HKANE,DIMM,W,WORK,LWORK,RWORK,INFO)

          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(2(A,F6.3,1X))')"RHO=",RC*RHO,"ZETA=",ZETA*ZC
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 

          DO I=1,6,2
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=HKANE(J,I)*HKANE(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(3)
           CARAC(3)=CARAC(2)
           CARAC(5)=CARAC(5)+CARAC(6)
           CARAC(6)=CARAC(5)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(5).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements were calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(5,5)
          ROOT(2)=HKANE(6,6) !Redundant

       
         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ENERGY(1) = POTE(CHI)+ZBC1(CHI)*SHID

         ENERGY(2) = ROOT(4)
         ENERGY(3) = ROOT(3)
         ENERGY(4) = ROOT(3)
         ENERGY(5) = ROOT(4)

         ENERGY(6) = ROOT(1)
         ENERGY(7) = ROOT(1)
!        ENERGY(7) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*(XLAMB*Tr(e)**2+XMU/2*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ENERGY(8) = XMU*(SXX**2 + SYY**2 + SZZ**2 + &
                           2.E0*(SXY**2+SXZ**2+SYZ**2) ) + &
                      XLAMB/2.E0*(SXX + SYY + SZZ)**2
          RETURN

      END SUBROUTINE POT_CALC_CAR_ZB

      SUBROUTINE POT_CALC_CYL_ZB(CHI,ENERGY)
      IMPLICIT NONE

        REAL, DIMENSION(:) :: ENERGY
        INTEGER, PARAMETER :: DIMM=6
        REAL, DIMENSION(1:DIMM,1:DIMM) :: HKANE
        REAL :: SSUM,SDIF,SZZ,SRZ,SRR,S00,SHID,STIL
        INTEGER :: I,J,CHI
!! DIAGONALIZATION VARIABLES

        REAL ::    W(1:DIMM), WORK(1:10*DIMM), AW(1:DIMM,1:DIMM), &
                   ROOT(DIMM), CARAC(DIMM), SLAMCH
        INTEGER :: IWORK(DIMM*5), IFAIL(DIMM), INFO, NUM


        IF(STR_Action.EQ.0) THEN
          SSUM=0.E0; SDIF=0.E0; SZZ=0.E0;
          SRZ=0.E0; SRR=0.E0; S00=0.E0
        ELSE
          SRR=EXX(I_X,I_Y,I_Z)
          S00=EYY(I_X,I_Y,I_Z)
          SSUM=EXX(I_X,I_Y,I_Z)+EYY(I_X,I_Y,I_Z)
          SDIF=EXX(I_X,I_Y,I_Z)-EYY(I_X,I_Y,I_Z)
          SZZ=EZZ(I_X,I_Y,I_Z)
          SRZ=EXZ(I_X,I_Y,I_Z)
          SHID=SSUM+SZZ
          STIL=SSUM-2.E0*SZZ
        END IF

         HKANE(1,1)=POTHH(CHI)+DVD(CHI)*SHID &
                    -1./3.*DVU(CHI)*STIL
         HKANE(2,2)=POTLH(CHI)+DVD(CHI)*SHID &
                    +1./3.*DVU(CHI)*STIL

         HKANE(3,3)=HKANE(2,2)
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=POTSO(CHI)+DSD(CHI)*SHID
         HKANE(6,6)=HKANE(5,5)

         IF(DIAG_Action.NE.0.AND.STR_Action.NE.0) THEN 
            HKANE(1,2)=2./SQRT(3.)*D2VU(CHI)*SRZ
            HKANE(1,3)=1./SQRT(3.)*(DVU(CHI)+D2VU(CHI))/2.*SDIF
            HKANE(1,4)=0.E0
            HKANE(1,5)=-SQRT(2./3.)*D2VSU(CHI)*SRZ
            HKANE(1,6)=-SQRT(2./3.)*(DVU(CHI)+D2VSU(CHI))/2.*SDIF
   
            HKANE(2,3)=0.E0
            HKANE(2,4)=HKANE(1,3)
            HKANE(2,5)=-SQRT(2.)/3.*DVSU(CHI)*STIL
            HKANE(2,6)=SQRT(2.)*D2VSU(CHI)*SRZ
   
            HKANE(3,4)=-HKANE(1,2)
            HKANE(3,5)=HKANE(2,6)
            HKANE(3,6)=-HKANE(2,5)
   
            HKANE(4,5)=-HKANE(1,6)
            HKANE(4,6)=HKANE(1,5)
   
            HKANE(5,6)=0

          DO I=1,6
           DO J=I+1,6
            HKANE(J,I)=HKANE(I,J)
           END DO
          END DO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Analytic solutions for Rho = 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!         ZETA = ZMIN+ZINC*IZ ! XZ normalized to ZC
!
!        W(1)=(HKANE(2,2)+HKANE(5,5))+
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        W(2)=(HKANE(2,2)+HKANE(5,5))-
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        write(24,'(4(f18.8,1x))')ZETA,HKANE(1,1),W(1)/2.,W(2)/2.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         
          CALL DSYEVX('V','A','U',DIMM,HKANE,DIMM,0.,0.,2,2, &
           2*SLAMCH('S'),NUM,W,AW,DIMM,WORK,10*DIMM,IWORK,IFAIL,INFO)
         
          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(A,10(I2,1X))')"IFAIL=",IFAIL(1:DIMM)
               write(6,'(2(A,F6.3,1X))')"RHO=",RC*RHO,"ZETA=",ZETA*ZC
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 

          DO I=1,6,2
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=AW(J,I)*AW(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(3)
           CARAC(3)=CARAC(2)
           CARAC(5)=CARAC(5)+CARAC(6)
           CARAC(6)=CARAC(5)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(5).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements were calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(5,5)
          ROOT(2)=HKANE(6,6) !Redundant

       
         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ENERGY(1) = POTE(CHI)+ZBC1(CHI)*SHID
                  
         ENERGY(2) = ROOT(4)
         ENERGY(3) = ROOT(3)
         ENERGY(4) = ROOT(3)
         ENERGY(5) = ROOT(4)
                  
         ENERGY(6) = ROOT(1)
         ENERGY(7) = ROOT(1)
!        ESODW(IR,IZ,IGEO) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*XLAMB*Tr(e)**2+XMU*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ENERGY(8) = XMU*(SRR**2 + S00**2 + SZZ**2 + SRZ**2) +    &
                      XLAMB/2.E0*(SSUM + SZZ)**2

          RETURN

      END SUBROUTINE POT_CALC_CYL_ZB

      END SUBROUTINE POTENTIAL_ZB

