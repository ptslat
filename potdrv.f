!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Program to draw the potential energy from a file containig the strain
! profiles
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



 
      PROGRAM POT_PROFILE

           Use Dot_Geometry
      
      IMPLICIT NONE

      INTEGER,PARAMETER :: DIMM=6
 
***** 'dummy' and local variables ************************************** 

      REAL            VWE,VWH,DW1,DW2,DW3,VBE,VBH,DB1,DB2,DB3
 
      INTEGER         Number_Param,IGEO,NWF
      REAL            VBIEL,VBIHH,VBILH,VBILS,VBISO
      REAL            POTWE,POTWHH,POTWLH,POTWLS,POTWSO,
     &                POTBE,POTBHH,POTBLH,POTBLS,POTBSO
      REAL            C1,C2,D1,D2,D3,D4,D5,D6,BISUM,BIZZ,BIAUX
      REAL            XMU,XLAMB,EPS0
      REAL            RC,ZC,DWL,RHO,ZETA,ZM

      INTEGER I,IIR,J,K,IZ,IR

!! GRID RESULTS OF STRAIN

      REAL,ALLOCATABLE,DIMENSION(:,:,:) :: MSUM, MDIF, MRR,
     &                                     M00,  MZZ,  MRZ,
     &                                     MHID, MTIL

!! GRID RESULTS OF PIEZOELECTRIC POTENTIAL

      REAL,ALLOCATABLE,DIMENSION(:,:,:) :: PTOT

!! GRID RESULTS OF POTENTIALS

      REAL,ALLOCATABLE,DIMENSION(:,:,:) :: EEL,   EHHUP, EHHDW,
     &                                     ELHUP, ELHDW, ESOUP,
     &                                     ESODW, ELAST

!! FRACTIONAL COORDINATES AND GEOM. PARAMETERS

      REAL,ALLOCATABLE :: XVALS(:), YVALS(:), RADI(:,:), ZETI(:,:), PAR(:)

!! HAMILTONIAN

      REAL         HKANE(1:DIMM,1:DIMM)
      INTEGER      CHI

!! DIAGONALIZATION VARIABLES

      REAL         W(1:DIMM), WORK(1:10*DIMM), AW(1:DIMM,1:DIMM),
     &             ROOT(DIMM), CARAC(DIMM), SLAMCH
      INTEGER      IWORK(DIMM*5), IFAIL(DIMM), INFO, NUM


!! DEFROMATION POTENTIALS AND ENERGY PROFILES
      REAL,DIMENSION(0:1) :: POTE, POTHH, POTLH, POTSO, POTLS,
     &                       PC1, PC2, PD1, PD2, PD3, PD4, PD5, PD6

      REAL         ERR,E00,EZZ,ERZ,ESUM,EDIF,PZO

!!!!!!! COMMON BLOCKS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      CHARACTER(LEN=120) :: input_str,input_pzo,output_eng
      COMMON /FILES/        input_str,input_pzo,output_eng

      REAL                  CARAC_MAX
      COMMON /CARAC/        CARAC_MAX

!! MORE INOUT.INC VARIABLES

      LOGICAL            :: DIAGLOG,PZOLOG,STRLOG,DWLLOG
      COMMON /LOGICS/       DIAGLOG,PZOLOG,STRLOG

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      INCLUDE    'INC/inout.inc'  

! Allocating Arrays

      ALLOCATE(MSUM(0:NWF,0:NWF,1:Number_Param),
     &         MDIF(0:NWF,0:NWF,1:Number_Param),
     &         MRR(0:NWF,0:NWF,1:Number_Param),
     &         M00(0:NWF,0:NWF,1:Number_Param),
     &         MZZ(0:NWF,0:NWF,1:Number_Param),
     &         MRZ(0:NWF,0:NWF,1:Number_Param),
     &         MHID(0:NWF,0:NWF,1:Number_Param),
     &         MTIL(0:NWF,0:NWF,1:Number_Param),
     &         PTOT(0:NWF,0:NWF,1:Number_Param))

      ALLOCATE(EEL(0:NWF,0:NWF,1:Number_Param),
     &         EHHUP(0:NWF,0:NWF,1:Number_Param),
     &         EHHDW(0:NWF,0:NWF,1:Number_Param),
     &         ELHUP(0:NWF,0:NWF,1:Number_Param),
     &         ELHDW(0:NWF,0:NWF,1:Number_Param), 
     &         ESOUP(0:NWF,0:NWF,1:Number_Param),
     &         ELAST(0:NWF,0:NWF,1:Number_Param),
     &         ESODW(0:NWF,0:NWF,1:Number_Param))

      ALLOCATE(XVALS(0:NWF),YVALS(0:NWF),RADI(1:Number_Param,1:3),
     &         ZETI(1:Number_Param,1:2),PAR(1:Number_Param))

! Definition of parameters for Barrier and Well

      VWE=VWE+DW1+DW2
      VBE=VBE+DB1+DB2

      POTWE  =  VWE 
      POTWHH = (VWH+DW1+DW2) 
      POTWLH = (VWH+(DW1-DW2+4.E0*DW3)/3.E0) 
      POTWSO = (VWH+2.E0*(DW1-DW2-2.E0*DW3)/3.E0)
      POTWLS = (DW1-DW2+DW3)
      POTBE  =  VBE 
      POTBHH = (VBH+DB1+DB2) 
      POTBLH = (VBH+(DB1-DB2+4.E0*DB3)/3.E0) 
      POTBSO = (VBH+2.E0*(DB1-DB2-2.E0*DB3)/3.E0)
      POTBLS = (DB1-DB2+DB3)

      IF(.NOT.STRLOG) THEN
       VBIEL=           ( C2*BISUM + C1*BIZZ )
       VBIHH=      ( (D2+D4)*BISUM + (D1+D3)*BIZZ )
       VBILH=   ( (D2+D4/3.)*BISUM + (D1+D3/3.)*BIZZ )
       VBISO=( (D2+2.*D4/3.)*BISUM + (D1+2.*D3/3.)*BIZZ )
       VBILS=           ( D4*BISUM + D3*BIZZ )
  
!      Potential edges including strain effect
       POTWE= (POTWE+VBIEL)
       POTWHH= (POTWHH+VBIHH)
       POTWLH= (POTWLH+VBILH)
       POTWSO= (POTWSO+VBISO)
       POTWLS= (POTWLS+VBILS)
      END IF

      POTE(0)  = POTBE  ; POTE(1)  = POTWE
      POTHH(0) = POTBHH ; POTHH(1) = POTWHH
      POTLH(0) = POTBLH ; POTLH(1) = POTWLH
      POTLS(0) = POTBLS ; POTLS(1) = POTWLS
      POTSO(0) = POTBSO ; POTSO(1) = POTWSO
      
! Deformation potentials for barrier and well are equal
      
      PC1(0) = C1 ; PC1(1) = C1
      PC2(0) = C2 ; PC2(1) = C2
      PD1(0) = D1 ; PD1(1) = D1
      PD2(0) = D2 ; PD2(1) = D2
      PD3(0) = D3 ; PD3(1) = D3
      PD4(0) = D4 ; PD4(1) = D4
      PD5(0) = D5 ; PD5(1) = D5
      PD6(0) = D6 ; PD6(1) = D6
      
      IF(STRLOG) THEN
      
       CALL STR_UNPACK(MSUM,MDIF,MRR,M00,MZZ,MRZ,MHID,MTIL,Number_Param,NWF,
     &               RADI,ZETI,PAR,XVALS,YVALS)
      ELSE
        
        MSUM=0.E0; MDIF=0.E0; MRR=0.E0; M00=0.E0; MZZ=0.E0; MRZ=0.E0
        MHID=0.E0; MTIL=0.E0

      END IF

      IF(PZOLOG) THEN

       CALL PZO_UNPACK(Number_Param,NWF,PTOT,RADI,ZETI,PAR,XVALS,YVALS)

      ELSE

       PTOT=0.E0

      END IF
      
      DO IGEO=1,Number_Param

        IIR=0

        Rqd_Base = RADI(IGEO,1)
        Rqd_Top  = RADI(IGEO,2)
        Hqd      = ZETI(IGEO,1)
        ZC       = ZETI(IGEO,2)
        RC       = RADI(IGEO,3)
        write(12,*)Rqd_Base,Rqd_Top,Hqd,ZC,RC

!! In case that the parameter would be DWL:
        IF(DWLLOG) DWL = PAR(IGEO)

        DO IR=NWF/2,NWF

         RHO     = XVALS(IR)*RC

         IF (RHO.LE.Rqd_Base) THEN
            CALL SHAPERTOZ(RHO,ZM)
            write(12,*)RHO,ZM
         ELSE
            ZM = 0.E0
            write(12,*)RHO,ZM
         END IF


        DO IZ=0,NWF
         
         ZETA   = YVALS(IZ)*ZC

         ERR    = MRR(IR,IZ,Number_Param)
         E00    = M00(IR,IZ,Number_Param)
         EZZ    = MZZ(IR,IZ,Number_Param)
         ERZ    = MRZ(IR,IZ,Number_Param)
         ESUM   = MSUM(IR,IZ,Number_Param)
         EDIF   = MDIF(IR,IZ,Number_Param)
         PZO    = PTOT(IR,IZ,Number_Param)

         CHI=0
         IF (ZETA.GE.-DWL .AND. ZETA.LE.ZM) CHI=1

         HKANE(1,1)=POTHH(CHI)-PZO+
     &               (PD1(CHI)+PD3(CHI))*EZZ+(PD2(CHI)+PD4(CHI))*ESUM 
         
         HKANE(2,2)=POTLH(CHI)-PZO+(PD1(CHI)+PD3(CHI)/3.E0)*EZZ+
     &               (PD2(CHI)+PD4(CHI)/3.E0)*ESUM
         HKANE(3,3)=HKANE(2,2)
         HKANE(4,4)=HKANE(1,1)
         HKANE(5,5)=POTSO(CHI)-PZO+(PD1(CHI)+2.E0*PD3(CHI)/3.E0)*EZZ+
     &               (PD2(CHI)+2.E0*PD4(CHI)/3.E0)*ESUM
         HKANE(6,6)=HKANE(5,5)

         IF(.NOT.DIAGLOG.AND.STRLOG) THEN 
         
          HKANE(1,2)=-SQRT(2.E0/3.E0)*PD6(CHI)*ERZ
          HKANE(1,3)=-1.E0/SQRT(3.E0)*PD5(CHI)*EDIF
          HKANE(1,4)=0.
          HKANE(1,5)=1.E0/SQRT(3.E0)*PD6(CHI)*ERZ
          HKANE(1,6)=SQRT(2./3.)*PD5(CHI)*EDIF
 
          HKANE(2,3)=0.
          HKANE(2,4)=HKANE(1,3)
          HKANE(2,5)=SQRT(2.E0)/3.E0*(POTLS(CHI)+(PD3(CHI)*EZZ+PD4(CHI)*ESUM))
          HKANE(2,6)=-PD6(CHI)*ERZ

          HKANE(3,4)=-HKANE(1,2)
          HKANE(3,5)=HKANE(2,6)
          HKANE(3,6)=-HKANE(2,5)
 
          HKANE(4,5)=-HKANE(1,6)
          HKANE(4,6)=HKANE(1,5)

          HKANE(5,6)=0



          DO I=1,6
           DO J=I+1,6
            HKANE(J,I)=HKANE(I,J)
           END DO
          END DO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Analytic solutions for Rho = 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!         ZETA = ZMIN+ZINC*IZ ! XZ normalized to ZC
!
!        W(1)=(HKANE(2,2)+HKANE(5,5))+
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        W(2)=(HKANE(2,2)+HKANE(5,5))-
!    & SQRT(  (HKANE(2,2)-HKANE(5,5))**2+4.*HKANE(3,6)**2  )
!        write(24,'(4(f18.8,1x))')ZETA,HKANE(1,1),W(1)/2.,W(2)/2.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         
          CALL SSYEVX('V','A','U',DIMM,HKANE,DIMM,0.,0.,2,2,
     &     2*SLAMCH('S'),NUM,W,AW,DIMM,WORK,10*DIMM,IWORK,IFAIL,INFO)
         
          IF (INFO.ne.0) then
               write(6,*)"Not Succesful Exit. Stopping. INFO=",INFO
               write(6,'(A,10(I2,1X))')"IFAIL=",IFAIL(1:DIMM)
               write(6,*)"IR=",IR," IZ=",IZ
!              STOP
          END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Ordering Eigenvalues according to the Bloch func. caracter 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ROOT = 0.E0 

          DO I=1,6,2
! Calculo de la componentes dependientes del spin
           DO J=1,6
            CARAC(J)=AW(J,I)*AW(J,I)
           END DO
! Suma de componentes independientemente del spin (hh_up + hh_dw, lh_up + lh_dw)
           CARAC(1)=CARAC(1)+CARAC(4)
           CARAC(4)=CARAC(1)
           CARAC(2)=CARAC(2)+CARAC(3)
           CARAC(3)=CARAC(2)
           CARAC(5)=CARAC(5)+CARAC(6)
           CARAC(6)=CARAC(5)
!
! Peso de las componentes de las que se extraeran los autovalores.
!
           IF (CARAC(1).GE.CARAC_MAX) THEN ! HH
                   ROOT(4)=W(I)
           END IF
           IF (CARAC(2).GE.CARAC_MAX) THEN ! LH
                   ROOT(3)=W(I)
           END IF
           IF (CARAC(5).GE.CARAC_MAX) THEN ! SO
                   ROOT(1)=W(I)
           END IF

          END DO

         ELSE ! Only the diagonal elements were calculated
       
          ROOT(4)=HKANE(1,1)
          ROOT(3)=HKANE(2,2)
          ROOT(1)=HKANE(5,5)
          ROOT(2)=HKANE(6,6) !Redundant

       
         END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! RESULTS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         EEL(IR,IZ,IGEO)   = POTE(CHI)-PZO+(PC1(CHI)*EZZ+PC2(CHI)*ESUM)

         EHHUP(IR,IZ,IGEO) = ROOT(4)
         ELHUP(IR,IZ,IGEO) = ROOT(3)
         ELHDW(IR,IZ,IGEO) = ROOT(3)
         EHHDW(IR,IZ,IGEO) = ROOT(4)

         ESOUP(IR,IZ,IGEO) = ROOT(1)
         ESODW(IR,IZ,IGEO) = ROOT(1)
!        ESODW(IR,IZ,IGEO) = CHI ! To save the Structure-profile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Calculation of the Elastic Energy. Since EEL contains no information
!!!!! we will use this array to pack the Elastic Energy.
!!!!!
!!!!!        U=1/2*XLAMB*Tr(e)**2+XMU*(err**2+e00**2+ezz**2+erz**2)
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          ELAST(IR,IZ,IGEO)=XMU*(ERR**2 + E00**2 + EZZ**2 + ERZ**2) +
     &                      0.5*XLAMB*(ERR + E00 + EZZ)**2

         IIR=1
        END DO

       END DO

      END DO


       DO IGEO=1,Number_Param
        DO IZ=0,NWF
         DO IR=0,NWF/2-1
          EEL(ir,iz,IGEO)=EEL(NWF-IR,iz,IGEO)
          EHHUP(ir,iz,IGEO)=EHHUP(NWF-IR,iz,IGEO)
          EHHDW(ir,iz,IGEO)=EHHDW(NWF-IR,iz,IGEO)
          ELHUP(ir,iz,IGEO)=ELHUP(NWF-IR,iz,IGEO)
          ELHDW(ir,iz,IGEO)=ELHDW(NWF-IR,iz,IGEO)
          ESOUP(ir,iz,IGEO)=ESOUP(NWF-IR,iz,IGEO)
          ESODW(ir,iz,IGEO)=ESODW(NWF-IR,iz,IGEO)
          ELAST(ir,iz,IGEO)=ELAST(NWF-IR,iz,IGEO)
         END DO
        END DO
       END DO

      CALL POT_PACK(EEL,EHHUP,ELHUP,ELHDW,EHHDW,
     &           ESOUP,ESODW,ELAST,Number_Param,NWF,RADI,ZETI,PAR,XVals,YVals)

      END PROGRAM POT_PROFILE
