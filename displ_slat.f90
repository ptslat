!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!      This subroutine calculates the strain distribution              !
!              inside and around the quantum dot                       !
!                                                                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SUBROUTINE DISPLACEMENT(UR,UX,UY,UZ)

              Use Input_Data
              Use Dot_Geometry
              Use Auxiliar_Procedures, ONLY : AISO
              Use DISPL_CALC
              Use STRAIN_CALC
              Use PZO_CALC
      
      IMPLICIT NONE

!!!!! 'dummy' and local variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      REAL         ZM,THETA,CTHETA,STHETA,AUXCHI,CHI,UUR,UUR2, &
                   UUX,UUY,UUZ,UUZ2,X,Y,Z,ZMAUX,EESUM,EEDIF,EE00,&
                   R_Profile
      REAL,DIMENSION(:,:,:) ::  UX,UY,UZ,UR
      INTEGER      I_X,I_Y,I_Z,I_N1,I_N2,I_N3

      REAL, DIMENSION(3) :: R_SL,X_VEC,XI_VEC

      !!! COMMON
      REAL :: RHO,ZETA,ETA
      COMMON /QAGON/RHO,ZETA,ETA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      UX=0.E0;UY=0.E0;UZ=0.E0;UR=0.E0

ZD:   DO I_Z=1,ZDim
        WRITE(16,'(A,I3,A,I3)')"I_Z ",I_Z," of ",ZDIM
        Z=Z_Min+REAL(I_Z-1)*Z_Inc
YD:   DO I_Y=1,YDim
        Y=Y_Min+REAL(I_Y-1)*Y_Inc
XD:   DO I_X=1,XDim
        X=X_Min+REAL(I_X-1)*X_Inc

        X_VEC=(/X,Y,Z/)

       UUX=0.E0;UUY=0.E0;UUZ=0.E0;UUZ2=0.E0;UUR=0.E0

N3:   DO I_N3=NMin_Z,NMax_Z
N2:   DO I_N2=NMin_Y,NMax_Y
N1:   DO I_N1=NMin_X,NMax_X

      R_SL=REAL(I_N1)*A1_S+REAL(I_N2)*A2_S+REAL(I_N3)*A3_S

      XI_VEC=X_VEC-R_SL

      RHO=SQRT(XI_VEC(1)**2+XI_VEC(2)**2)/RC
      IF(XI_VEC(1).EQ.0.E0.AND.XI_VEC(2).EQ.0.E0) THEN
              CTHETA=1.E0/SQRT(2.E0); STHETA=1.E0/SQRT(2.E0) ! It is not the Mathematical limit
      ELSE
              THETA=ATAN(XI_VEC(2)/XI_VEC(1))
              CTHETA=Cos(THETA); STHETA=Sin(THETA)
      END IF
      ZETA=XI_VEC(3)/ZC

          IF (RHO.LE.RD) THEN
             CALL SHAPERTOZ(MIN(RHO*RC,Rqd_Base),ZMAUX)
             ZM=ZMAUX/ZC
          ELSE
             ZM = 0.E0
          END IF

    
          IF (abs(zeta) .EQ. 0.E0 .OR. ZETA .EQ. ZM) THEN 
              ZETA=ZETA-1.E-5
          END IF

          CHI = 0.
          IF (RHO.LE.RD.AND.ZETA.GE.0.E0.AND.ZETA.LE.ZM) THEN
                  CHI = 1.
                  if(I_N1.NE.0.OR.I_N2.NE.0.OR.I_N3.NE.0) THEN
                          WRITE(16,*)I_N1,I_N2,I_N3
                          WRITE(16,*)X_VEC(3),ZETA*ZC,ZM*ZC
                  END IF
          END IF

          IF (AISO.EQ.1) THEN
             CALL DSPLISO(EESUM,EEDIF,UUZ2,CHI)
          ELSE
             CALL DSPLANISO(EESUM,EEDIF,UUZ2,CHI)
          END IF
          
           IF (RHO .EQ. 0.E0) EEDIF = 0.E0
   
           EE00  = (EESUM-EEDIF)/2.E0
           UUR2=RHO*EE00


!!!!!!!!!!!!!!!!!! Wetting Layer !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          IF(I_N1.EQ.0.AND.I_N2.EQ.0.AND.I_N3.EQ.0) THEN

!          IF (ZETA.LT.0.E0 .AND. ZETA.GE.-D) THEN

               IF (AISO.EQ.1) THEN
                  UUZ2 = UUZ2-BIAUX/2.E0* &
                         (ZC/RC)*((ZETA-(-D))*SIGN(1.E0,ZETA-(-D))-ZETA*SIGN(1.E0,ZETA))
               ELSE
                  UUZ2 = UUZ2-(2.E0*C13/C33*EPSA+EPSC)/2.E0* &
                         (ZC/RC)*((ZETA-(-D))*SIGN(1.E0,ZETA-(-D))-ZETA*SIGN(1.E0,ZETA))
               END IF

!          END IF
          END IF
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

           UUX = UUX + CTHETA*RC*UUR2
           UUY = UUY + STHETA*RC*UUR2
           UUR = UUR + RC*UUR2
           UUZ = UUZ + RC*UUZ2

      END DO N1
      END DO N2
      END DO N3

           UX(I_X,I_Y,I_Z)= UUX
           UY(I_X,I_Y,I_Z)= UUY
           UR(I_X,I_Y,I_Z)= UUR
           UZ(I_X,I_Y,I_Z)= UUZ
         
!           WRITE(26,'(10(E15.8,1X))')Z,EEXX,EEYY,EEZZ,EEXY,EEXZ,EEYZ
       
      END DO XD
      END DO YD
      END DO ZD
      
!     STOP

      RETURN
      END SUBROUTINE DISPLACEMENT

       

