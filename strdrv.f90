!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Driver for stain0.f subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
  PROGRAM STRAINDRV

        Use Dot_Geometry
        Use Auxiliar_Procedures
        Use Input_Data
        Use NCPACK
      
      IMPLICIT NONE

!! GRID RESULTS OF STRAIN0

      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: EXX,EYY,EZZ,EXZ,EYZ,EXY

      INTERFACE
       SUBROUTINE STRAIN0(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXY,EXZ,EYZ 
       END SUBROUTINE
      END INTERFACE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      CALL READ_INPUT()

      ALLOCATE (EXX(1:XDim,1:YDim,1:ZDim),EYY(1:XDim,1:YDim,1:ZDim), &
                EZZ(1:XDim,1:YDim,1:ZDim),EXZ(1:XDim,1:YDim,1:ZDim), &  
                EXY(1:XDim,1:YDim,1:ZDim),EYZ(1:XDim,1:YDim,1:ZDim) )
                

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Quantum Dot Dimensions (A)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


       RC=Rqd_Base
       ZC=Hqd


       RD  = Rqd_Base/RC    ! RD = Rqd_Base normalized to RC
       HD  = Hqd/ZC         ! HD = HQD normalized to ZC 

       D=DWL/ZC ! Wetting layer thick. normalized to ZC

       CALL STRAIN0(EXX,EYY,EZZ,EXY,EXZ,EYZ)


       IF (KCOOR.EQ.0) THEN
           CALL NCPACK_CART(EXX,EYY,EZZ,EXY,EXZ,EYZ)
       ELSE
           CALL NCPACK_CYL(EXX,EYY,EZZ,EXZ)
       END IF


  END PROGRAM STRAINDRV
