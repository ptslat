!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Driver for stain0.f subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
  PROGRAM PZODRV

        Use Dot_Geometry
        Use Auxiliar_Procedures
        Use Input_Data
        Use NCPACK
      
      IMPLICIT NONE

!! GRID RESULTS OF STRAIN0

      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: P_SPONT,P_PIEZO

      INTERFACE
       SUBROUTINE PIEZO(P_SPONT,P_PIEZO)
         REAL,DIMENSION(:,:,:) ::  P_SPONT,P_PIEZO
       END SUBROUTINE
      END INTERFACE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      CALL READ_INPUT()

      ALLOCATE (P_SPONT(1:XDim,1:YDim,1:ZDim),&
                P_PIEZO(1:XDim,1:YDim,1:ZDim) )
                

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Quantum Dot Dimensions (A)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


       RC=Rqd_Base+23.E0
       ZC=Hqd+23.E0


       RD  = Rqd_Base/RC    ! RD = Rqd_Base normalized to RC
       HD  = Hqd/ZC         ! HD = HQD normalized to ZC 

       D=DWL/ZC ! Wetting layer thick. normalized to ZC

       CALL PIEZO(P_SPONT,P_PIEZO)
       CALL NCPACK_PZ(P_SPONT,P_PIEZO)


  END PROGRAM PZODRV
