
   MODULE Auxiliar_Procedures
   
     INTEGER, SAVE :: AISO  !AISO=0, Anisotropic EZZ and ESUM
     INTEGER, SAVE :: MTYPE !MTYPE: 1-> ZBI, 2-> WZI, 3-> WZA

   CONTAINS  

    SUBROUTINE Param_Var(Ind_Par,Dep_Par,Step,Relation)
    IMPLICIT NONE
    
     REAL, INTENT(INOUT)              :: Ind_Par
     REAL, OPTIONAL, INTENT(INOUT)    :: Dep_Par
     REAL, INTENT(IN)                 :: Step
     CHARACTER (LEN = * ), INTENT(IN) :: Relation
    

     SELECT CASE (TRIM(Relation))

       CASE('Single')

               Ind_Par = Ind_Par + Step
       CASE('Ratio')

               IF(.NOT. PRESENT(Dep_Par) ) THEN
                       write(6,*)"Box_Size error. Optional argument Dep_Par Needed!"
                       STOP
               END IF
               Dep_Par   = Dep_Par/Ind_Par * (Ind_Par+Step)
               Ind_Par = Ind_Par + Step

       CASE('Both')

               IF(.NOT. PRESENT(Dep_Par) ) THEN
                       write(6,*)"Box_Size error. Optional argument Dep_Par Needed!"
                       STOP
               END IF
               Dep_Par = Dep_Par + Step
               Ind_Par = Ind_Par + Step
                      
      END SELECT

      RETURN

     END SUBROUTINE Param_Var


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!             Integration: Simpson's method (version REAL)             !
!                          NX mus be even                              !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      REAL FUNCTION RSIMPSON(F,XINC,NX)
      IMPLICIT NONE
 
!!!!! variables 'dummy' y variables internas !!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      INTEGER      NX,I
      REAL         F(0:NX),XINC
 
      RSIMPSON = 0.E0
      DO I = 1,NX-1,2
         RSIMPSON = RSIMPSON+F(I)
      END DO
 
      RSIMPSON = 2.E0*RSIMPSON
 
      DO I = 2,NX-2,2
         RSIMPSON = RSIMPSON+F(I)
      END DO
 
      RSIMPSON = 2.E0*RSIMPSON+F(0)+F(NX)
      RSIMPSON = XINC*RSIMPSON/3.E0
 
      RETURN
      END FUNCTION RSIMPSON

 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                     Kronecker's delta                                !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      INTEGER FUNCTION IDELTA(I,J)
      IMPLICIT NONE
     
!!!!! 'dummy' and local variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      INTEGER      I,J

      IDELTA = 0
      IF (I.EQ.J) THEN
         IDELTA = 1    
      END IF   
      RETURN
      END FUNCTION IDELTA
 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Internal subroutine - checks error status after each netcdf, prints out text message each time
! an error code is returned. 
      SUBROUTINE check(status)
         USE NETCDF, ONLY: nf90_noerr, nf90_strerror
        integer, intent ( in) :: status
        
        if(status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
        end if
      END SUBROUTINE check  
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                            !!
!! Subrutina para cambiar el nombre de un archivo existente y asi evitar      !!
!! sobrescribirlo.                                                            !!
!!                                                                            !!
!! ARCH: cadena con el nombre de archivo original.                            !!
!! Mira la longitud de la variable por si admite un contador de cuatro        !!
!! digitos "_XXX". Si lo admite le a�ade al nombre de archivo ese contador.   !!
!! Si no lo admite sustrae un caracter y le a�ade un contador simple "X".     !!
!!                                                                            !!
!! Ver: 1.0 Date: 17/04/03                                                    !!
!!                                                                            !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE OPEN_OVER(ARCH)
      IMPLICIT NONE
    
       CHARACTER (LEN=*) :: ARCH
       CHARACTER (LEN=120) :: ARCH_TMP
       LOGICAL :: EX_FILE
       INTEGER :: I
    
       INQUIRE(FILE=TRIM(ARCH),EXIST=EX_FILE)
       IF (.NOT.EX_FILE) RETURN
    
       IF(LEN(ARCH).LT.(LEN_TRIM(ARCH)+4)) THEN
               I=1
               DO WHILE (.TRUE.)
    
                WRITE(ARCH_TMP,'(A,I1)')ARCH(1:LEN(ARCH)-1),I
                INQUIRE(FILE=TRIM(ARCH_TMP),EXIST=EX_FILE)
                IF (.NOT.EX_FILE) THEN
                 ARCH=TRIM(ARCH_TMP)
                 WRITE(0,*)"Error. Trying to overwrite a file"
                 WRITE(0,'(T5,A,A)')"FILE NAME CHANGED TO: ",TRIM(ARCH)
                 EXIT
                ELSE
                 I=I+1
                ENDIF
    
               END DO
        END IF
    
       I=1
       DO WHILE (.TRUE.)
    
        WRITE(ARCH_TMP,'(A,"_",I3.3)')TRIM(ARCH),I
        INQUIRE(FILE=TRIM(ARCH_TMP),EXIST=EX_FILE)
        IF (.NOT.EX_FILE) THEN
                ARCH=TRIM(ARCH_TMP)
                WRITE(0,*)"Error. Trying to overwrite a file"
                WRITE(0,'(T5,A,A)')"FILE NAME CHANGED TO: ",TRIM(ARCH)
                EXIT
        ELSE
                I=I+1
        ENDIF
    
       END DO
    
       RETURN
    
       END SUBROUTINE OPEN_OVER

    END MODULE Auxiliar_Procedures
