  MODULE Input_Data
  IMPLICIT NONE

  REAL, SAVE  :: A_S,B_S,C_S ! -> Superlattice constants
  REAL, DIMENSION(3), SAVE :: A1_S,A2_S,A3_S ! -> Superlattice vectors
  INTEGER, SAVE :: NMin_X,NMax_X,NMin_Y,NMax_Y,NMin_Z,NMax_Z
  REAL, SAVE    :: X_Min,X_Max,Y_Min,Y_Max,Z_Min,Z_Max
  REAL, SAVE    :: X_Inc,Y_Inc,Z_Inc
  INTEGER, SAVE :: XDim,YDim,ZDim
  INTEGER, SAVE :: KCOOR
  INTEGER,PRIVATE :: NDots_X,NDots_Y,NDots_Z

  !!! Material Parameters
  REAL, SAVE   :: XLAMB,XMU,C13,C33,C11,EPSC,EPSA
  !!! Calculation Constants
  REAL, SAVE   :: ETA1,ETA2,ETA_DIF,CN31_1,CN31_2,&
                  BISUM,BIZZ,BIAUX

  !!! Normalized Dot Parameters
  REAL, SAVE :: RC,ZC,D,RD,HD

  !!! PIEZOELECTRIC CONSTANTS
  REAL, PRIVATE :: PZE15,PZE31,PZE33,PSPB,PSPW,DIELEC
  REAL, SAVE    :: CI1,CI2,CI3
  REAL, SAVE    :: CNE2_1,CNE2_2,CNE_1,CNE_2,CSP,CPZ
  REAL, SAVE    :: CSPWL,CSPBR,CPZWL,CPZ_Q1,CPZ_Q2

  CONTAINS
  
  SUBROUTINE READ_INPUT()

      Use Dot_Geometry
      Use Auxiliar_Procedures, ONLY : AISO, MTYPE
  
  IMPLICIT NONE

      INTEGER :: G_Method
      REAL    :: D_Z
      REAL,DIMENSION(3) :: V_AUX
      


!!!!! read data from data file !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
      READ (*,*) 
      READ (*,*)
      READ (*,*)
      READ (*,*)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      READ (*,*)
      READ (*,*) DWL            ! Wetting layer thickness (A)
      READ (*,*) ISHAPE         ! Shape of the dot
      READ (*,*) HQD            ! Height of the quantum dot (A) 
      READ (*,*) Rqd_Base       ! Base Radius (A)
      READ (*,*) Rqd_Top        ! Top Radius (A)
!!! Superlattice Begins !!!!
      READ (*,*)  
      READ (*,*) A_S,B_S,C_S
      READ (*,*) A1_S(1),A1_S(2),A1_S(3)
      READ (*,*) A2_S(1),A2_S(2),A2_S(3)
      READ (*,*) A3_S(1),A3_S(2),A3_S(3)
      READ (*,*) NDots_X,NDots_Y,NDots_Z
      V_AUX=(/A_S,B_S,C_S/)
      A1_S=A1_S*V_AUX
      A2_S=A2_S*V_AUX
      A3_S=A3_S*V_AUX
      IF(MOD(NDots_X,2).EQ.0) THEN
              NDots_X=NDots_X+1
              WRITE(6,'(A,1X,I3)')"Warning: NDots_X Even -> NDots_X=",NDots_X
      END IF
      IF(MOD(NDots_Y,2).EQ.0) THEN
              NDots_Y=NDots_Y+1
              WRITE(6,'(A,1X,I3)')"Warning: NDots_Y Even -> NDots_Y=",NDots_Y
      END IF
      IF(MOD(NDots_Z,2).EQ.0) THEN
              NDots_Z=NDots_Z+1
              WRITE(6,'(A,1X,I3)')"Warning: NDots_Z Even -> NDots_Z=",NDots_Z
      END IF
      IF(NDots_X.EQ.1) THEN
              NMin_X=0; NMax_X=0
      ELSE
              NMin_X=-(NDots_X-1)/2; NMax_X=-NMin_X
      END IF
      IF(NDots_Y.EQ.1) THEN
              NMin_Y=0; NMax_Y=0
      ELSE
              NMin_Y=-(NDots_Y-1)/2; NMax_Y=-NMin_Y
      END IF
      IF(NDots_Z.EQ.1) THEN
              NMin_Z=0; NMax_Z=0
      ELSE
              NMin_Z=-(NDots_Z-1)/2; NMax_Z=-NMin_Z
      END IF
!!! Grid Begins !!!!
      READ (*,*)
      READ (*,*) G_Method
      READ (*,*) X_Min,X_Max
      READ (*,*) Y_Min,Y_Max
      READ (*,*) Z_Min,Z_Max
      READ (*,*) XDim,YDim,ZDim

      IF(G_Method.EQ.1) THEN
           IF(XDim.EQ.1) THEN
              X_Max=X_Min
           ELSE
              X_Max=A1_S(1)/2.E0
              X_Min=0.E0
           END IF
           IF(YDim.EQ.1) THEN
              Y_Max=Y_Min
           ELSE
              Y_Max=A2_S(2)/2.E0
              Y_Min=0.E0
           END IF
           IF(ZDim.EQ.1) THEN
              Z_Max=Z_Min
           ELSE
              Z_Max=A3_S(3)/2.E0
              D_Z=Z_Max-(Hqd+DWL)/2.E0
              Z_Max=Hqd+D_Z
              Z_Min=-DWL-D_Z
           END IF
      END IF
      
      IF (XDim.EQ.1) THEN
              X_Inc=0.E0
      ELSE
              X_Inc=(X_Max-X_Min)/REAL(XDim-1)
      END IF
      IF (YDim.EQ.1) THEN
              Y_Inc=0.E0
      ELSE
              Y_Inc=(Y_Max-Y_Min)/REAL(YDim-1)
      END IF
      IF (ZDim.EQ.1) THEN
              Z_Inc=0.E0
      ELSE
              Z_Inc=(Z_Max-Z_Min)/REAL(ZDim-1)
      END IF
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      READ (*,*)
      READ (*,*) C11,C13,C33    ! Elastic modulus
      READ (*,*) XLAMB,XMU      ! Lame constants 
      READ (*,*) EPSA           ! Misfit strain: EPS0 
      READ (*,*) EPSC           ! Misfit strain: EPSC 
      READ (*,*) PZE15,PZE31,PZE33 !Piezoelectric Constants
      READ (*,*) PSPB,PSPW      ! Spontaneous Polarization
      READ (*,*) DIELEC         ! Barrier Dielectric Constant
      READ (*,*) MTYPE          ! MTYPE: 1-> WZI, 2-> WZA
      IF (MTYPE.LT.2) THEN
!      C13=XLAMB; C11=XLAMB+2.E0*XMU; C33=C11
!      EPSC=EPSA
       AISO=1
!      WRITE(6,*)"ERROR: WZ ISO NOT IMPLEMENTED"
!      STOP
      ELSE
       AISO=0
      END IF
      READ (*,*) KCOOR          ! KCOOR=0 Cartesian Coordinates
                                ! KCOOR=1 Cylindrical Coordinates
      IF(KCOOR.EQ.1.AND.YDIM.GT.1) THEN
         WRITE(6,*)"ERROR: Selected coordinates are cylindrical and YDIM.GT.1"
         WRITE(6,*)"       Exiting Program"
         STOP
      END IF
      READ (*,*)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      RETURN

    END SUBROUTINE READ_INPUT

    SUBROUTINE CONSTANTS( )
        Use Dot_Geometry, ONLY: DWL
        Use Auxiliar_Procedures, ONLY : AISO
      IMPLICIT NONE

      REAL :: ALPHA,BETA,GAMA
      REAL :: R,S,A1,A2,B1,B2
      REAL :: CN3_1,CN3_2,P1,P2,P3
      REAL :: ETA_AUX, PI

      PI=4.E0*ATAN(1.E0)

!!!! Remember: XLAMB=C12, XMU=C44 !!!!!

      !!! ISO CONSTANTS
      BISUM =  2.*EPSA          ! Biaxial strain components
      BIZZ  = -2.*EPSA*XLAMB/(XLAMB+2.*XMU)
      BIAUX =  EPSA-BIZZ

      R=(C11+XLAMB+C13)*EPSA+C13*(EPSC-EPSA)
      S=(C33+C13-XLAMB-C11)*EPSA+(C33-C13)*(EPSC-EPSA)

      ALPHA=-XMU*C33

      R=R/ALPHA; S=S/ALPHA

      A1=R*(C33-XMU-C13)-S*(C13+XMU)
      A2=R*XMU

      B1=S*(C13+2.E0*XMU)-R*(C33-C13-2.E0*XMU)
      B2=S*C11-R*(C13+2.E0*XMU-C11)

      BETA=( C13*(C13+2.E0*XMU)-C11*C33 )/(-XMU*C33)
      GAMA=C11/C33

      CN3_1=A1+B1;      CN3_2=A2+B2

      P1=2.E0*PZE15; P2=PZE31; P3=PZE33

      CI1=2.E0*EPSA*P2+EPSC*P3
      CI2=P3
      CI3=1.E0

      CNE_1=(P1+P2-P3)*A1+(P1/2-P3)*B1
      CNE_2=(P1+P2-P3)*A2+(P1/2-P3)*B2

      ETA_AUX=SQRT(BETA**2 - 4.E0*GAMA)
      ETA1=(-BETA+ETA_AUX)/2.E0
      ETA2=(-BETA-ETA_AUX)/2.E0
      IF(ETA1.GT.0.E0.OR.ETA2.GT.0.E0) THEN
        WRITE(6,*)"ERROR: ETA1 AND ETA2 ARE GREATHER THAN ZERO"
        STOP
      END IF
      ETA1=ABS(ETA1)
      ETA2=ABS(ETA2)

      ETA_DIF=ETA1-ETA2

      CN31_1=(CN3_2/SQRT(ETA2)-CN3_1*SQRT(ETA2))
      CN31_2=(CN3_2/SQRT(ETA1)-CN3_1*SQRT(ETA1))
      
      CNE2_1=(1.E0-ETA1)*(CNE_2/SQRT(ETA2)-CNE_1*SQRT(ETA2))
      CNE2_2=(1.E0-ETA2)*(CNE_2/SQRT(ETA1)-CNE_1*SQRT(ETA1))
      
      CSP=(PSPW-PSPB)
      !!! V=q/(4*PI*E0)*PZ=0.9*PZ eV
      CPZ=0.9*(4.E0*PI/DIELEC)
      CPZ_Q1=(BISUM*PZE31+BIZZ*PZE33)
      CPZ_Q2=(BIAUX*(PZE33-(PZE31+2.E0*PZE15)))

!!!   Wetting Layer Parameters

      IF(NDots_z.GT.1) THEN
        CSPWL=CSP*(C_S-DWL)/C_S
        CSPBR=-CSP*(DWL/C_S)
      ELSE
        CSPWL=CSP
        CSPBR=0.E0
      END IF
        
        IF(AISO.EQ.0) THEN
           CPZWL=(PZE31-PZE33*(C13/C33))*BISUM
        ELSE
           CPZWL=(BISUM*PZE31+BIZZ*PZE33)
        END IF
      
      RETURN
    
    END SUBROUTINE CONSTANTS


  END MODULE INPUT_DATA


