MODULE NCPACK_DPL
         Use typeSizes
         Use NETCDF
         Use Dot_Geometry, ONLY : Rqd_Base,Rqd_Top,Hqd,ISHAPE
         Use Input_Data
         Use Auxiliar_Procedures, ONLY : MTYPE, check, open_over
  IMPLICIT NONE

  INTEGER,PRIVATE :: ncFileID, HdVarID, RtVarID, RbVarID, NDots_XVarID, &
                     NDots_YVarID,NDots_ZVarID, A1VarID, A2VarID, A3VarID, &
                     DWL_VarID, Dim3ID


CONTAINS

  SUBROUTINE NCPACK_DPL_CART(UX,UY,UZ)

 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  UX,UY,UZ
 
    ! netcdf related variables
    integer :: DimXID, DimYID, DimZID,i,  &
               uxVarID, uyVarID, uzVarID, &
               DimXVarID, DimYVarID, DimZVarID
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = DPL_Filename
 
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call open_over(fileName) !Check if a file with the same name exists
    
    call check(nf90_create(path = trim(fileName), cmode = nf90_clobber, ncid = ncFileID))
    
    CALL NCPACK_INPUT_DPL( )
    
    ! Define the dimensions
    call check(nf90_def_dim(ncid = ncFileID, name = "dimx",     len = XDim,        dimid = DimXID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimy",     len = YDim,        dimid = DimYID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimz",     len = ZDim,        dimid = DimZID))
 
    ! Create variables and attributes
 
    ! Box sizes
 
 
    call check(nf90_def_var(ncFileID, "dimx", nf90_float, dimids = DimXID, varID = DimXVarID) )
    call check(nf90_put_att(ncFileID, DimXVarID, "long_name", "X dimension"))
    call check(nf90_put_att(ncFileID, DimXVarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "dimy", nf90_float, dimids = DimYID, varID = DimYVarID) )
    call check(nf90_put_att(ncFileID, DimYVarID, "long_name", "Y dimension"))
    call check(nf90_put_att(ncFileID, DimYVarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "dimz", nf90_float, dimids = DimZID, varID = DimZVarID) )
    call check(nf90_put_att(ncFileID, DimZVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimZVarID, "units",     "Angstroms"))
 
 
    ! strain grids
    call check(nf90_def_var(ncid = ncFileID, name = "ux", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = uxVarID) )
    call check(nf90_put_att(ncFileID, uxVarID, "long_name",   "ux"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "uy", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = uyVarID) )
    call check(nf90_put_att(ncFileID, uyVarID, "long_name",   "uy"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "uz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = uzVarID) )
    call check(nf90_put_att(ncFileID, uxVarID, "long_name",   "uz"))
 
    ! In the C++ interface the define a scalar variable - do we know how to do this? 
    !call check(nf90_def_var(ncFileID, "ScalarVariable", nf90_real, scalarVarID))
    
    ! Leave define mode
    call check(nf90_enddef(ncfileID))
    
    ! Write the dimension variables
 
    call check(nf90_put_var(ncFileID, DimXVarId,(/(X_Min+REAL(I-1)*X_Inc, i=1,XDim)/)) )
    call check(nf90_put_var(ncFileID, DimYVarId,(/(Y_Min+REAL(I-1)*Y_Inc, i=1,YDim)/)) )
    call check(nf90_put_var(ncFileID, DimZVarId,(/(Z_Min+REAL(I-1)*Z_Inc, i=1,ZDim)/)) )
 
 
    ! Write the Strain distributions
    
    call check(nf90_put_var(ncFileID, uxVarID, ux) )
    call check(nf90_put_var(ncFileID, uyVarID, uy) )
    call check(nf90_put_var(ncFileID, uzVarID, uz) )
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCPACK_DPL_CART

  SUBROUTINE NCPACK_DPL_CYL(UR,UZ)
 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  UR,UZ
 
    ! netcdf related variables
    integer :: DimXID, DimZID,i,  &
               urVarID, uzVarID,DimXVarID, DimZVarID

    real, dimension(1:XDim,1:ZDim) :: U_AUX
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = DPL_Filename
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call open_over(fileName) !Check if a file with the same name exists
    
    call check(nf90_create(path = trim(fileName), cmode = nf90_clobber, ncid = ncFileID))
    
    CALL NCPACK_INPUT_DPL( )
    
    ! Define the dimensions
    call check(nf90_def_dim(ncid = ncFileID, name = "dimx",     len = XDim,        dimid = DimXID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimz",     len = ZDim,        dimid = DimZID))
 
    ! Create variables and attributes
 
    ! Box sizes
 
    call check(nf90_def_var(ncFileID, "dimx", nf90_float, dimids = DimXID, varID = DimXVarID) )
    call check(nf90_put_att(ncFileID, DimXVarID, "long_name", "Radial dimension"))
    call check(nf90_put_att(ncFileID, DimXVarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "dimz", nf90_float, dimids = DimZID, varID = DimZVarID) )
    call check(nf90_put_att(ncFileID, DimZVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimZVarID, "units",     "Angstroms"))
 
 
    ! strain grids
    call check(nf90_def_var(ncid = ncFileID, name = "ur", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = urVarID) )
    call check(nf90_put_att(ncFileID, urVarID, "long_name",   "ur"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "uz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = uzVarID) )
    call check(nf90_put_att(ncFileID, uzVarID, "long_name",   "uz"))
 
    ! Leave define mode
    call check(nf90_enddef(ncfileID))
    
    ! Write the dimension variables
 
    call check(nf90_put_var(ncFileID, DimXVarId,     (/(X_Min+REAL(I-1)*X_Inc, i=1,XDim)/)) )
    call check(nf90_put_var(ncFileID, DimZVarId,     (/(Z_Min+REAL(I-1)*Z_Inc, i=1,ZDim)/)) )
 
 
    ! Write the Strain distributions
    
    U_AUX=UR(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, urVarID, U_AUX) )

    U_AUX=UZ(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, uzVarID, U_AUX) )

 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCPACK_DPL_CYL

  SUBROUTINE NCPACK_INPUT_DPL( )
    IMPLICIT NONE

    CHARACTER(LEN=200) :: stitle

    call check(nf90_def_dim(ncid = ncFileID, name = "vector",   len = 3,           dimid = Dim3ID))

    call check(nf90_def_var(ncid = ncFileID, name = "HD", xtype = nf90_float,     &
                            varID = HdVarID) )
    call check(nf90_put_att(ncFileID, HdVarID, "long_name",   "Dot Height (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RT", xtype = nf90_float,     &
                            varID = RtVarID) )
    call check(nf90_put_att(ncFileID, RtVarID, "long_name",   "Top Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RB", xtype = nf90_float,     &
                            varID = RbVarID ) )
    call check(nf90_put_att(ncFileID, RbVarID, "long_name",   "Base Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_X", xtype = nf90_int,     &
                            varID = NDots_XVarID ) )
    call check(nf90_put_att(ncFileID, NDots_XVarID, "long_name",   "Number of Dots along X-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Y", xtype = nf90_int,     &
                            varID = NDots_YVarID ) )
    call check(nf90_put_att(ncFileID, NDots_YVarID, "long_name",   "Number of Dots along Y-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Z", xtype = nf90_int,     &
                            varID = NDots_ZVarID ) )
    call check(nf90_put_att(ncFileID, NDots_ZVarID, "long_name",   "Number of Dots along Z-axis"))

    call check(nf90_def_var(ncFileID, "A1", nf90_float, dimids = Dim3ID, varID = A1VarID) )
    call check(nf90_put_att(ncFileID, A1VarID, "long_name", "Vector A1 Superlattice"))
    call check(nf90_put_att(ncFileID, A1VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A2", nf90_float, dimids = Dim3ID, varID = A2VarID) )
    call check(nf90_put_att(ncFileID, A2VarID, "long_name", "Vector A2 Superlattice"))
    call check(nf90_put_att(ncFileID, A2VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A3", nf90_float, dimids = Dim3ID, varID = A3VarID) )
    call check(nf90_put_att(ncFileID, A3VarID, "long_name", "Vector A3 Superlattice"))
    call check(nf90_put_att(ncFileID, A3VarID, "units",     "Angstroms"))

    call check(nf90_def_var(ncFileID, "DWL", nf90_float, varID = DWL_VarID))

    SELECT CASE(ISHAPE)
      CASE(1)
        stitle="Shape: Truncated Cone. "
      CASE(2)
        stitle="Shape: Cap. "
    END SELECT

    SELECT CASE(MTYPE)
      CASE(1) !! Isotropic Zincblende
        stitle="Material Type: Isotropic Zincblende. "//stitle
      CASE(2) !! Isotropic Wurtzite
        stitle="Material Type: Isotropic Wurtzite. "//stitle
      CASE(3) !! Anisotropic Wurtzite
        stitle="Material Type: Anisotropic Wurtzite. "//stitle
    END SELECT

    ! Global attributes
    call check(nf90_put_att(ncFileID, nf90_global, "history", &
                       "NetCDF file"))
    stitle="Displacement distribution. "//stitle
    
    call check(nf90_put_att(ncFileID, nf90_global, "title", TRIM(stitle)))
    
    call check(nf90_enddef(ncfileID))

    call check(nf90_put_var(ncFileID, RbVarId, Rqd_Base ) )
    call check(nf90_put_var(ncFileID, RtVarId, Rqd_Top ) )
    call check(nf90_put_var(ncFileID, HdVarId, Hqd ) )
    call check(nf90_put_var(ncFileID, DWL_VarId, D*ZC ) )
    call check(nf90_put_var(ncFileID, NDots_XVarId, NMax_X-NMin_X+1 ) )
    call check(nf90_put_var(ncFileID, NDots_YVarId, NMax_Y-NMin_Y+1 ) )
    call check(nf90_put_var(ncFileID, NDots_ZVarId, NMax_Z-NMin_Z+1 ) )
    call check(nf90_put_var(ncFileID, A1VarId, A1_S ) )
    call check(nf90_put_var(ncFileID, A2VarId, A2_S ) )
    call check(nf90_put_var(ncFileID, A3VarId, A3_S ) )


    call check(nf90_redef(ncfileID))

    RETURN

  END SUBROUTINE NCPACK_INPUT_DPL


END MODULE NCPACK_DPL

