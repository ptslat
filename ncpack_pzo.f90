MODULE NCPACK_PZO
         Use typeSizes
         Use NETCDF
         Use Dot_Geometry, ONLY : Rqd_Base,Rqd_Top,Hqd,ISHAPE
         Use Input_Data
         Use Auxiliar_Procedures, ONLY : MTYPE, check, open_over
  IMPLICIT NONE

  INTEGER,PRIVATE :: ncFileID, HdVarID, RtVarID, RbVarID, NDots_XVarID, &
                     NDots_YVarID,NDots_ZVarID, A1VarID, A2VarID, A3VarID, &
                     DWL_VarID, Dim3ID


CONTAINS

  SUBROUTINE NCPACK_PZ(P_SPONT,P_PIEZO)
 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  P_SPONT,P_PIEZO
 
    ! netcdf related variables
    integer :: DimXID, DimYID, DimZID,i,  &
               pspVarID, ppzVarID, pttVarID,&
               DimXVarID, DimYVarID, DimZVarID
                              
    ! Local variables
    character (len = 60) :: fileName
 
    real, dimension(1:XDim,1:ZDim) :: PZO_AUX

    !-----
    ! Initialization
    !-----
 
    fileName = PZO_Filename
 
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call open_over(fileName) !Check if a file with the same name exists
    
    call check(nf90_create(path = trim(fileName), cmode = nf90_clobber, ncid = ncFileID))
    
    CALL NCPACK_INPUT_PZO( )
    
    ! Define the dimensions
    call check(nf90_def_dim(ncid = ncFileID, name = "dimx",     len = XDim,        dimid = DimXID))
    IF(KCOOR.EQ.0) &
    call check(nf90_def_dim(ncid = ncFileID, name = "dimy",     len = YDim,        dimid = DimYID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimz",     len = ZDim,        dimid = DimZID))
 
    ! Create variables and attributes
 
    ! Box sizes
 
 
    call check(nf90_def_var(ncFileID, "dimx", nf90_float, dimids = DimXID, varID = DimXVarID) )
    call check(nf90_put_att(ncFileID, DimXVarID, "long_name", "Radial dimension"))
    call check(nf90_put_att(ncFileID, DimXVarID, "units",     "Angstroms"))
 
    IF (KCOOR.EQ.0) THEN
    call check(nf90_def_var(ncFileID, "dimy", nf90_float, dimids = DimYID, varID = DimYVarID) )
    call check(nf90_put_att(ncFileID, DimYVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimYVarID, "units",     "Angstroms"))
    END IF
 
    call check(nf90_def_var(ncFileID, "dimz", nf90_float, dimids = DimZID, varID = DimZVarID) )
    call check(nf90_put_att(ncFileID, DimZVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimZVarID, "units",     "Angstroms"))
 
 
    ! potential grids
    SELECT CASE(KCOOR)
    CASE(0)
       call check(nf90_def_var(ncid = ncFileID, name = "P_SP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = pspVarID) )
       call check(nf90_put_att(ncFileID, pspVarID, "long_name", "Potential SP"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "P_PZ", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = ppzVarID) )
       call check(nf90_put_att(ncFileID, ppzVarID, "long_name",   "Potential PZ"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "P_TOT", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = pttVarID) )
       call check(nf90_put_att(ncFileID, pttVarID, "long_name",   "Total Potential"))
    CASE(1)
       call check(nf90_def_var(ncid = ncFileID, name = "P_SP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = pspVarID) )
       call check(nf90_put_att(ncFileID, pspVarID, "long_name", "Potential SP"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "P_PZ", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = ppzVarID) )
       call check(nf90_put_att(ncFileID, ppzVarID, "long_name",   "Potential PZ"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "P_TOT", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = pttVarID) )
       call check(nf90_put_att(ncFileID, pttVarID, "long_name",   "Total Potential"))
    END SELECT 
    ! In the C++ interface the define a scalar variable - do we know how to do this? 
    !call check(nf90_def_var(ncFileID, "ScalarVariable", nf90_real, scalarVarID))
    
    ! Leave define mode
    call check(nf90_enddef(ncfileID))
    
    ! Write the dimension variables
 
    call check(nf90_put_var(ncFileID, DimXVarId,     (/(X_Min+REAL(I-1)*X_Inc, i=1,XDim)/)) )
    IF(KCOOR.EQ.0) &
    call check(nf90_put_var(ncFileID, DimYVarId,     (/(Y_Min+REAL(I-1)*Y_Inc, i=1,YDim)/)) )
    call check(nf90_put_var(ncFileID, DimZVarId,     (/(Z_Min+REAL(I-1)*Z_Inc, i=1,ZDim)/)) )
 
 
    ! Write the Strain distributions
    
    SELECT CASE(KCOOR)
    CASE(0)
      call check(nf90_put_var(ncFileID, pspVarID, P_SPONT) )
      call check(nf90_put_var(ncFileID, ppzVarID, P_PIEZO) )
      call check(nf90_put_var(ncFileID, pttVarID, P_SPONT+P_PIEZO) )
    CASE(1)
      PZO_AUX=P_SPONT(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, pspVarID, PZO_AUX) )
      PZO_AUX=P_PIEZO(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, ppzVarID, PZO_AUX) )
      PZO_AUX=P_SPONT(1:XDim,1,1:ZDim)+P_PIEZO(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, pttVarID, PZO_AUX) )
    END SELECT
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCPACK_PZ

  SUBROUTINE NCREAD_PZ(P_SPONT,P_PIEZO)

 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  P_SPONT,P_PIEZO
 
    ! netcdf related variables
    integer :: DimXID, DimYID, DimZID,  &
               pspVarID, ppzVarID
               

    integer :: NcDimX,NcDimY,NcDimZ

    real, dimension(1:XDim,1:ZDim) :: PZO_AUX
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = PZO_Filename
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call check(nf90_open(path = trim(fileName), mode = nf90_nowrite, ncid = ncFileID))
    
    ! Define the dimensions
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimx", dimid = DimXID))
    IF(KCOOR.EQ.0) &
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimy", dimid = DimYID))
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimz", dimid = DimZID))
 
    ! Create variables and attributes
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimXID, len = NcDimX))
    IF(KCOOR.EQ.0) &
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimYID, len = NcDimY))
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimZID, len = NcDimZ)) 
    
    IF(KCOOR.EQ.0) THEN
       if ((NcDimX.ne.XDim).or.(NcDimY.ne.YDim).or.(NcDimZ.ne.ZDim)) then
               write(6,*)"Dimensions problem:"
               write(6,*)"Dimensions of the NetCDF File: ",TRIM(filename)
               write(6,*)"DimX:",NcDimX
               write(6,*)"DimY:",NcDimY
               write(6,*)"DimZ:",NcDimZ
               write(6,*)"Dimensions defined on actual run:"
               write(6,*)"DimX-Y-Z:",XDim,YDim,ZDim
               STOP
       end if
     ELSE
       if ((NcDimX.ne.XDim).or.(NcDimZ.ne.ZDim)) then
               write(6,*)"Dimensions problem:"
               write(6,*)"Dimensions of the NetCDF File: ",TRIM(filename)
               write(6,*)"DimX:",NcDimX
               write(6,*)"DimZ:",NcDimZ
               write(6,*)"Dimensions defined on actual run:"
               write(6,*)"DimX-Y-Z:",XDim,ZDim
               STOP
       end if
     END IF

  call check(nf90_inq_varid(ncid = ncFileID, name = "P_SP",     &
                     varID = pspVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "P_PZ",     &
                     varID = ppzVarID) )
 
    ! Read the Strain distributions
    
    SELECT CASE(KCOOR)
    CASE(0)
      call check(nf90_get_var(ncFileID, pspVarID, P_SPONT) )
      call check(nf90_get_var(ncFileID, ppzVarID, P_PIEZO) )
    CASE(1)
       call check(nf90_get_var(ncFileID, pspVarID, PZO_AUX) )
       P_SPONT(1:XDim,1,1:ZDim)=PZO_AUX
       call check(nf90_get_var(ncFileID, ppzVarID, PZO_AUX) )
       P_PIEZO(1:XDim,1,1:ZDim)=PZO_AUX
    END SELECT
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCREAD_PZ

  SUBROUTINE NCPACK_INPUT_PZO( )
    IMPLICIT NONE

    CHARACTER(LEN=200) :: stitle

    call check(nf90_def_dim(ncid = ncFileID, name = "vector",   len = 3,           dimid = Dim3ID))

    call check(nf90_def_var(ncid = ncFileID, name = "HD", xtype = nf90_float,     &
                            varID = HdVarID) )
    call check(nf90_put_att(ncFileID, HdVarID, "long_name",   "Dot Height (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RT", xtype = nf90_float,     &
                            varID = RtVarID) )
    call check(nf90_put_att(ncFileID, RtVarID, "long_name",   "Top Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RB", xtype = nf90_float,     &
                            varID = RbVarID ) )
    call check(nf90_put_att(ncFileID, RbVarID, "long_name",   "Base Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_X", xtype = nf90_int,     &
                            varID = NDots_XVarID ) )
    call check(nf90_put_att(ncFileID, NDots_XVarID, "long_name",   "Number of Dots along X-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Y", xtype = nf90_int,     &
                            varID = NDots_YVarID ) )
    call check(nf90_put_att(ncFileID, NDots_YVarID, "long_name",   "Number of Dots along Y-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Z", xtype = nf90_int,     &
                            varID = NDots_ZVarID ) )
    call check(nf90_put_att(ncFileID, NDots_ZVarID, "long_name",   "Number of Dots along Z-axis"))

    call check(nf90_def_var(ncFileID, "A1", nf90_float, dimids = Dim3ID, varID = A1VarID) )
    call check(nf90_put_att(ncFileID, A1VarID, "long_name", "Vector A1 Superlattice"))
    call check(nf90_put_att(ncFileID, A1VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A2", nf90_float, dimids = Dim3ID, varID = A2VarID) )
    call check(nf90_put_att(ncFileID, A2VarID, "long_name", "Vector A2 Superlattice"))
    call check(nf90_put_att(ncFileID, A2VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A3", nf90_float, dimids = Dim3ID, varID = A3VarID) )
    call check(nf90_put_att(ncFileID, A3VarID, "long_name", "Vector A3 Superlattice"))
    call check(nf90_put_att(ncFileID, A3VarID, "units",     "Angstroms"))

    call check(nf90_def_var(ncFileID, "DWL", nf90_float, varID = DWL_VarID))

    SELECT CASE(ISHAPE)
      CASE(1)
        stitle="Shape: Truncated Cone. "
      CASE(2)
        stitle="Shape: Cap. "
    END SELECT

    SELECT CASE(MTYPE)
      CASE DEFAULT !! Anisotropic Wurtzite
        stitle="Material Type: Anisotropic Wurtzite. "//stitle
    END SELECT

    ! Global attributes
    call check(nf90_put_att(ncFileID, nf90_global, "history", &
                       "NetCDF file"))
    stitle="Electrostatic Potential. "//stitle
    
    call check(nf90_put_att(ncFileID, nf90_global, "title", TRIM(stitle)))
    
    call check(nf90_enddef(ncfileID))

    call check(nf90_put_var(ncFileID, RbVarId, Rqd_Base ) )
    call check(nf90_put_var(ncFileID, RtVarId, Rqd_Top ) )
    call check(nf90_put_var(ncFileID, HdVarId, Hqd ) )
    call check(nf90_put_var(ncFileID, DWL_VarId, D*ZC ) )
    call check(nf90_put_var(ncFileID, NDots_XVarId, NMax_X-NMin_X+1 ) )
    call check(nf90_put_var(ncFileID, NDots_YVarId, NMax_Y-NMin_Y+1 ) )
    call check(nf90_put_var(ncFileID, NDots_ZVarId, NMax_Z-NMin_Z+1 ) )
    call check(nf90_put_var(ncFileID, A1VarId, A1_S ) )
    call check(nf90_put_var(ncFileID, A2VarId, A2_S ) )
    call check(nf90_put_var(ncFileID, A3VarId, A3_S ) )


    call check(nf90_redef(ncfileID))

    RETURN

  END SUBROUTINE NCPACK_INPUT_PZO

END MODULE NCPACK_PZO

