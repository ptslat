!
   MODULE Dot_Geometry
   IMPLICIT NONE

     REAL, SAVE    :: Rqd_Base, Rqd_Top, Hqd, DWL
     REAL, ALLOCATABLE, DIMENSION (:), SAVE :: XR, XZ
     INTEGER, SAVE :: Ishape

   CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                 Profile of the quantum dot                           !
!                                                                      !
!                   ISHAPE = 1   ! TRUNCATED CONE                      !
!                                ! Rqd_Top=0        -> CONE            !
!                                ! Rqd_Top=Rqd_Base -> CYLINDER        !
!                   ISHAPE = 2   ! CAP                                 !
!                                                                      !
!               SHAPERTOZ calculates XZ (given XR)                     !
!               SHAPEZTOR calculates XR (given XZ)                     !
!                                                                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE SHAPERTOZ(XR,XZ)
      IMPLICIT NONE

      REAL, INTENT(IN   ) :: XR
      REAL, INTENT(  OUT) :: XZ

      REAL  :: R0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      IF (XR.GT.Rqd_Base) THEN
         WRITE(6,*) 'ERROR IN SHAPERTOZ: XR OUTSIDE THE DOT'
         WRITE(6,*) XR,Rqd_Base
         STOP
         RETURN
      END IF   

      SELECT CASE (ISHAPE)

        CASE(1)
                IF (XR.LE.Rqd_Top) THEN
                   XZ = Hqd
                ELSE
                   XZ = - ( Hqd/(Rqd_Base-Rqd_Top) )*(XR-Rqd_Top)+Hqd
                END IF

        CASE(2)
                IF (XR.LE.Rqd_Top) THEN
                   XZ = Hqd
                ELSE
                   R0 = Rqd_Top**2+((Rqd_Base**2-Rqd_Top**2+Hqd**2)/(2.E0*Hqd))**2
                   XZ = sqrt(R0 -XR**2)-Sqrt(R0-Rqd_Top**2)+Hqd
                END IF

      END SELECT
      
      RETURN
      END SUBROUTINE SHAPERTOZ
 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE SHAPEZTOR(XR,XZ)
      IMPLICIT NONE

      REAL, INTENT(  OUT) :: XR
      REAL, INTENT(IN   ) :: XZ

      REAL  :: R0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      IF (XZ.GT.Hqd) THEN
         WRITE(6,*) 'ERROR IN SHAPERTOZ: XR OUTSIDE THE DOT'
         WRITE(6,*) XZ,Hqd
         STOP
         RETURN
      END IF   

      SELECT CASE (ISHAPE)

        CASE(1)
                   XR = - ( (Rqd_Base-Rqd_Top)/Hqd )*XZ + Rqd_Base

        CASE(2)
                   R0 = Rqd_Top**2+((Rqd_Base**2-Rqd_Top**2+Hqd**2)/(2.E0*Hqd))**2
                   XR = SQRT( R0-( XZ-Hqd+SQRT(R0-Rqd_Top**2) )**2 )
      END SELECT
      
      RETURN
      END SUBROUTINE SHAPEZTOR

     END MODULE Dot_Geometry
