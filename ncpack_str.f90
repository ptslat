MODULE NCPACK_STR
         Use typeSizes
         Use NETCDF
         Use Dot_Geometry, ONLY : Rqd_Base,Rqd_Top,Hqd,ISHAPE
         Use Input_Data
         Use Auxiliar_Procedures, ONLY : MTYPE, check, open_over
  IMPLICIT NONE

  INTEGER,PRIVATE :: ncFileID, HdVarID, RtVarID, RbVarID, NDots_XVarID, &
                     NDots_YVarID,NDots_ZVarID, A1VarID, A2VarID, A3VarID, &
                     DWL_VarID, Dim3ID


CONTAINS

  SUBROUTINE NCPACK_CART(EXX,EYY,EZZ,EXY,EXZ,EYZ)

 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXY,EXZ,EYZ 
 
    ! netcdf related variables
    integer :: DimXID, DimYID, DimZID,i,  &
               exxVarID, eyyVarID, ezzVarID, exyVarID, exzVarID, eyzVarID, &
               ehidVarID,etilVarID,DimXVarID, DimYVarID, DimZVarID
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = STR_Filename
 
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call open_over(fileName) !Check if a file with the same name exists
    
    call check(nf90_create(path = trim(fileName), cmode = nf90_clobber, ncid = ncFileID))
    
    CALL NCPACK_INPUT_STR( )
    
    ! Define the dimensions
    call check(nf90_def_dim(ncid = ncFileID, name = "dimx",     len = XDim,        dimid = DimXID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimy",     len = YDim,        dimid = DimYID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimz",     len = ZDim,        dimid = DimZID))
 
    ! Create variables and attributes
 
    ! Box sizes
 
 
    call check(nf90_def_var(ncFileID, "dimx", nf90_float, dimids = DimXID, varID = DimXVarID) )
    call check(nf90_put_att(ncFileID, DimXVarID, "long_name", "X dimension"))
    call check(nf90_put_att(ncFileID, DimXVarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "dimy", nf90_float, dimids = DimYID, varID = DimYVarID) )
    call check(nf90_put_att(ncFileID, DimYVarID, "long_name", "Y dimension"))
    call check(nf90_put_att(ncFileID, DimYVarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "dimz", nf90_float, dimids = DimZID, varID = DimZVarID) )
    call check(nf90_put_att(ncFileID, DimZVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimZVarID, "units",     "Angstroms"))
 
 
    ! strain grids
    call check(nf90_def_var(ncid = ncFileID, name = "exx", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = exxVarID) )
    call check(nf90_put_att(ncFileID, exxVarID, "long_name",   "exx"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "eyy", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = eyyVarID) )
    call check(nf90_put_att(ncFileID, eyyVarID, "long_name",   "eyy"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ezz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = ezzVarID) )
    call check(nf90_put_att(ncFileID, ezzVarID, "long_name",   "ezz"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "exy", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = exyVarID) )
    call check(nf90_put_att(ncFileID, exyVarID, "long_name",   "exy"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "exz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = exzVarID) )
    call check(nf90_put_att(ncFileID, exzVarID, "long_name",   "exz"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "eyz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = eyzVarID) )
    call check(nf90_put_att(ncFileID, eyzVarID, "long_name",   "eyz"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ehid", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = ehidVarID) )
    call check(nf90_put_att(ncFileID, ehidVarID, "long_name",   "Hidrostatic Strain"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "etil", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimYID, DimZID /), varID = etilVarID) )
    call check(nf90_put_att(ncFileID, etilVarID, "long_name",   "etil = exx + eyy - 2 * ezz"))
 
    ! In the C++ interface the define a scalar variable - do we know how to do this? 
    !call check(nf90_def_var(ncFileID, "ScalarVariable", nf90_real, scalarVarID))
    
    ! Leave define mode
    call check(nf90_enddef(ncfileID))
    
    ! Write the dimension variables
 
    call check(nf90_put_var(ncFileID, DimXVarId,     (/(X_Min+REAL(I-1)*X_Inc, i=1,XDim)/)) )
    call check(nf90_put_var(ncFileID, DimYVarId,     (/(Y_Min+REAL(I-1)*Y_Inc, i=1,YDim)/)) )
    call check(nf90_put_var(ncFileID, DimZVarId,     (/(Z_Min+REAL(I-1)*Z_Inc, i=1,ZDim)/)) )
 
 
    ! Write the Strain distributions
    
    call check(nf90_put_var(ncFileID, exxVarID, exx) )
    call check(nf90_put_var(ncFileID, eyyVarID, eyy) )
    call check(nf90_put_var(ncFileID, ezzVarID, ezz) )
    call check(nf90_put_var(ncFileID, exyVarID, exy) )
    call check(nf90_put_var(ncFileID, exzVarID, exz) )
    call check(nf90_put_var(ncFileID, eyzVarID, eyz) )
    call check(nf90_put_var(ncFileID, ehidVarID, exx+eyy+ezz) )
    call check(nf90_put_var(ncFileID, etilVarID, exx+eyy-2.E0*ezz) )
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCPACK_CART

  SUBROUTINE NCREAD_CART(EXX,EYY,EZZ,EXY,EXZ,EYZ)

 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXY,EXZ,EYZ 
 
    ! netcdf related variables
    integer :: DimXID, DimYID, DimZID,  &
               exxVarID, eyyVarID, ezzVarID, exyVarID, exzVarID, eyzVarID
               

    integer :: NcDimX,NcDimY,NcDimZ
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = STR_Filename
 
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call check(nf90_open(path = trim(fileName), mode = nf90_nowrite, ncid = ncFileID))
    
    ! Define the dimensions
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimx", dimid = DimXID))
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimy", dimid = DimYID))
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimz", dimid = DimZID))
 
    ! Create variables and attributes
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimXID, len = NcDimX))
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimYID, len = NcDimY))
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimZID, len = NcDimZ)) 
    
    if ((NcDimX.ne.XDim).or.(NcDimY.ne.YDim).or.(NcDimZ.ne.ZDim)) then
            write(6,*)"Dimensions problem:"
            write(6,*)"Dimensions of the NetCDF File: ",TRIM(filename)
            write(6,*)"DimX:",NcDimX
            write(6,*)"DimY:",NcDimY
            write(6,*)"DimZ:",NcDimZ
            write(6,*)"Dimensions defined on actual run:"
            write(6,*)"DimX-Y-Z:",XDim,YDim,ZDim
            STOP
    end if
 
 
  call check(nf90_inq_varid(ncid = ncFileID, name = "exx",     &
                     varID = exxVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "eyy",     &
                     varID = eyyVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "ezz",     &
                     varID = ezzVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "exy",     &
                     varID = exyVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "exz",     &
                     varID = exzVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "eyz",     &
                     varID = eyzVarID) )
 
    ! Read the Strain distributions
    
    call check(nf90_get_var(ncFileID, exxVarID, exx) )
    call check(nf90_get_var(ncFileID, eyyVarID, eyy) )
    call check(nf90_get_var(ncFileID, ezzVarID, ezz) )
    call check(nf90_get_var(ncFileID, exyVarID, exy) )
    call check(nf90_get_var(ncFileID, exzVarID, exz) )
    call check(nf90_get_var(ncFileID, eyzVarID, eyz) )
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCREAD_CART


  SUBROUTINE NCPACK_CYL(EXX,EYY,EZZ,EXZ)
 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXZ 
 
    ! netcdf related variables
    integer :: DimXID, DimZID,i,  &
               esumVarID, errVarID, e00VarID, edifVarID, ezzVarID, erzVarID, &
               ehidVarID,etilVarID,DimXVarID, DimZVarID

    real, dimension(1:XDim,1:ZDim) :: STR_AUX
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = STR_Filename
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call open_over(fileName) !Check if a file with the same name exists
    
    call check(nf90_create(path = trim(fileName), cmode = nf90_clobber, ncid = ncFileID))
    
    CALL NCPACK_INPUT_STR( )
    
    ! Define the dimensions
    call check(nf90_def_dim(ncid = ncFileID, name = "dimx",     len = XDim,        dimid = DimXID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimz",     len = ZDim,        dimid = DimZID))
 
    ! Create variables and attributes
 
    ! Box sizes
 
    call check(nf90_def_var(ncFileID, "dimx", nf90_float, dimids = DimXID, varID = DimXVarID) )
    call check(nf90_put_att(ncFileID, DimXVarID, "long_name", "Radial dimension"))
    call check(nf90_put_att(ncFileID, DimXVarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "dimz", nf90_float, dimids = DimZID, varID = DimZVarID) )
    call check(nf90_put_att(ncFileID, DimZVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimZVarID, "units",     "Angstroms"))
 
 
    ! strain grids
    call check(nf90_def_var(ncid = ncFileID, name = "esum", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = esumVarID) )
    call check(nf90_put_att(ncFileID, esumVarID, "long_name",   "err+e00"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "err", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = errVarID) )
    call check(nf90_put_att(ncFileID, errVarID, "long_name",   "err"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "e00", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = e00VarID) )
    call check(nf90_put_att(ncFileID, e00VarID, "long_name",   "e00"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ezz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = ezzVarID) )
    call check(nf90_put_att(ncFileID, ezzVarID, "long_name",   "ezz"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "edif", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = edifVarID) )
    call check(nf90_put_att(ncFileID, edifVarID, "long_name",   "edif"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "erz", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = erzVarID) )
    call check(nf90_put_att(ncFileID, erzVarID, "long_name",   "erz"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ehid", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = ehidVarID) )
    call check(nf90_put_att(ncFileID, ehidVarID, "long_name",   "Hidrostatic Strain"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "etil", xtype = nf90_float,     &
                       dimids = (/ DimXID, DimZID /), varID = etilVarID) )
    call check(nf90_put_att(ncFileID, etilVarID, "long_name",   "etil = exx + eyy - 2 * ezz"))
 
    ! Leave define mode
    call check(nf90_enddef(ncfileID))
    
    ! Write the dimension variables
 
    call check(nf90_put_var(ncFileID, DimXVarId,     (/(X_Min+REAL(I-1)*X_Inc, i=1,XDim)/)) )
    call check(nf90_put_var(ncFileID, DimZVarId,     (/(Z_Min+REAL(I-1)*Z_Inc, i=1,ZDim)/)) )
 
 
    ! Write the Strain distributions
    
    STR_AUX=EXX(1:XDim,1,1:ZDim)+EYY(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, esumVarID, STR_AUX) )

    STR_AUX=EXX(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, errVarID, STR_AUX) )

    STR_AUX=EYY(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, e00VarID, STR_AUX) )

    STR_AUX=EZZ(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, ezzVarID, STR_AUX) )

    STR_AUX=EXX(1:XDim,1,1:ZDim)-EYY(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, edifVarID, STR_AUX) )

    STR_AUX=EXZ(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, erzVarID, STR_AUX) )

    STR_AUX=EXX(1:XDim,1,1:ZDim)+EYY(1:XDim,1,1:ZDim)+EZZ(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, ehidVarID, STR_AUX) )

    STR_AUX=EXX(1:XDim,1,1:ZDim)+EYY(1:XDim,1,1:ZDim)+2.*EZZ(1:XDim,1,1:ZDim)
    call check(nf90_put_var(ncFileID, etilVarID, STR_AUX) )
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCPACK_CYL

  SUBROUTINE NCREAD_CYL(EXX,EYY,EZZ,EXZ)

 
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXZ 
 
    ! netcdf related variables
    integer :: DimXID, DimZID,  &
               errVarID, e00VarID, ezzVarID, erzVarID
               

    real, dimension(1:XDim,1:ZDim) :: STR_AUX

    integer :: NcDimX,NcDimZ
                              
    ! Local variables
    character (len = 60) :: fileName
 
    !-----
    ! Initialization
    !-----
 
    fileName = STR_Filename
 
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call check(nf90_open(path = trim(fileName), mode = nf90_nowrite, ncid = ncFileID))
    
    ! Define the dimensions
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimx", dimid = DimXID))
    call check(nf90_inq_dimid(ncid = ncFileID, name = "dimz", dimid = DimZID))
 
    ! Create variables and attributes
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimXID, len = NcDimX))
    call check(nf90_Inquire_Dimension(ncid = ncFileID, dimid = DimZID, len = NcDimZ)) 
    
    if ((NcDimX.ne.XDim).or.(NcDimZ.ne.ZDim)) then
            write(6,*)"Dimensions problem:"
            write(6,*)"Dimensions of the NetCDF File: ",TRIM(filename)
            write(6,*)"DimX:",NcDimX
            write(6,*)"DimZ:",NcDimZ
            write(6,*)"Dimensions defined on actual run:"
            write(6,*)"DimX-Z:", XDim,ZDim
            STOP
    end if
 
 
  call check(nf90_inq_varid(ncid = ncFileID, name = "err",     &
                     varID = errVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "e00",     &
                     varID = e00VarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "ezz",     &
                     varID = ezzVarID) )
  call check(nf90_inq_varid(ncid = ncFileID, name = "erz",     &
                     varID = erzVarID) )
 
    ! Read the Strain distributions
    
    call check(nf90_get_var(ncFileID, errVarID, STR_AUX) )
    EXX(1:XDim,1,1:ZDim)=STR_AUX
    call check(nf90_get_var(ncFileID, e00VarID, STR_AUX) )
    EYY(1:XDim,1,1:ZDim)=STR_AUX
    call check(nf90_get_var(ncFileID, ezzVarID, STR_AUX) )
    EZZ(1:XDim,1,1:ZDim)=STR_AUX
    call check(nf90_get_var(ncFileID, erzVarID, STR_AUX) )
    EXZ(1:XDim,1,1:ZDim)=STR_AUX
 
    call check(nf90_close(ncFileID))
    
    RETURN
  
  END SUBROUTINE NCREAD_CYL 

  SUBROUTINE NCPACK_INPUT_STR( )
    IMPLICIT NONE

    CHARACTER(LEN=200) :: stitle

    call check(nf90_def_dim(ncid = ncFileID, name = "vector",   len = 3,           dimid = Dim3ID))

    call check(nf90_def_var(ncid = ncFileID, name = "HD", xtype = nf90_float,     &
                            varID = HdVarID) )
    call check(nf90_put_att(ncFileID, HdVarID, "long_name",   "Dot Height (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RT", xtype = nf90_float,     &
                            varID = RtVarID) )
    call check(nf90_put_att(ncFileID, RtVarID, "long_name",   "Top Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RB", xtype = nf90_float,     &
                            varID = RbVarID ) )
    call check(nf90_put_att(ncFileID, RbVarID, "long_name",   "Base Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_X", xtype = nf90_int,     &
                            varID = NDots_XVarID ) )
    call check(nf90_put_att(ncFileID, NDots_XVarID, "long_name",   "Number of Dots along X-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Y", xtype = nf90_int,     &
                            varID = NDots_YVarID ) )
    call check(nf90_put_att(ncFileID, NDots_YVarID, "long_name",   "Number of Dots along Y-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Z", xtype = nf90_int,     &
                            varID = NDots_ZVarID ) )
    call check(nf90_put_att(ncFileID, NDots_ZVarID, "long_name",   "Number of Dots along Z-axis"))

    call check(nf90_def_var(ncFileID, "A1", nf90_float, dimids = Dim3ID, varID = A1VarID) )
    call check(nf90_put_att(ncFileID, A1VarID, "long_name", "Vector A1 Superlattice"))
    call check(nf90_put_att(ncFileID, A1VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A2", nf90_float, dimids = Dim3ID, varID = A2VarID) )
    call check(nf90_put_att(ncFileID, A2VarID, "long_name", "Vector A2 Superlattice"))
    call check(nf90_put_att(ncFileID, A2VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A3", nf90_float, dimids = Dim3ID, varID = A3VarID) )
    call check(nf90_put_att(ncFileID, A3VarID, "long_name", "Vector A3 Superlattice"))
    call check(nf90_put_att(ncFileID, A3VarID, "units",     "Angstroms"))

    call check(nf90_def_var(ncFileID, "DWL", nf90_float, varID = DWL_VarID))

    SELECT CASE(ISHAPE)
      CASE(1)
        stitle="Shape: Truncated Cone. "
      CASE(2)
        stitle="Shape: Cap. "
    END SELECT

    SELECT CASE(MTYPE)
      CASE(1) !! Isotropic Zincblende
        stitle="Material Type: Isotropic Zincblende. "//stitle
      CASE(2) !! Isotropic Wurtzite
        stitle="Material Type: Isotropic Wurtzite. "//stitle
      CASE(3) !! Anisotropic Wurtzite
        stitle="Material Type: Anisotropic Wurtzite. "//stitle
    END SELECT

    ! Global attributes
    call check(nf90_put_att(ncFileID, nf90_global, "history", &
                       "NetCDF file"))
    stitle="Strain distribution. "//stitle
    
    call check(nf90_put_att(ncFileID, nf90_global, "title", TRIM(stitle)))
    
    call check(nf90_enddef(ncfileID))

    call check(nf90_put_var(ncFileID, RbVarId, Rqd_Base ) )
    call check(nf90_put_var(ncFileID, RtVarId, Rqd_Top ) )
    call check(nf90_put_var(ncFileID, HdVarId, Hqd ) )
    call check(nf90_put_var(ncFileID, DWL_VarId, D*ZC ) )
    call check(nf90_put_var(ncFileID, NDots_XVarId, NMax_X-NMin_X+1 ) )
    call check(nf90_put_var(ncFileID, NDots_YVarId, NMax_Y-NMin_Y+1 ) )
    call check(nf90_put_var(ncFileID, NDots_ZVarId, NMax_Z-NMin_Z+1 ) )
    call check(nf90_put_var(ncFileID, A1VarId, A1_S ) )
    call check(nf90_put_var(ncFileID, A2VarId, A2_S ) )
    call check(nf90_put_var(ncFileID, A3VarId, A3_S ) )


    call check(nf90_redef(ncfileID))

    RETURN

  END SUBROUTINE NCPACK_INPUT_STR


END MODULE NCPACK_STR

