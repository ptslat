!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Driver for stain0.f subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
  PROGRAM POTDRV

        Use Dot_Geometry
        Use Auxiliar_Procedures
        Use Input_Data
        Use NCPACK_PZO
        Use NCPACK_STR
        Use NCPACK_POT
        Use NCPACK_DPL
      
      IMPLICIT NONE

!! GRID RESULTS OF STRAIN0

      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: P_SPONT,P_PIEZO, &
                                             EXX,EYY,EZZ,EXZ,EYZ,EXY, &
                                             EEL,EHHUP,EHHDW,ELHUP, &
                                             ELHDW,ESOUP,ESODW,ELAST,&
                                             UX,UY,UZ,UR

  
      INTERFACE
       SUBROUTINE PIEZO(P_SPONT,P_PIEZO)
         REAL,DIMENSION(:,:,:) ::  P_SPONT,P_PIEZO
       END SUBROUTINE
       SUBROUTINE STRAIN0(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         REAL,DIMENSION(:,:,:) ::  EXX,EYY,EZZ,EXY,EXZ,EYZ 
       END SUBROUTINE
       SUBROUTINE DISPLACEMENT(UR,UX,UY,UZ)
         REAL,DIMENSION(:,:,:) :: UR,UX,UY,UZ
       END SUBROUTINE
       SUBROUTINE POTENTIAL_ZB(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                               ESOUP,ESODW,ELAST,EXX,EYY,&
                               EZZ,EXY,EXZ,EYZ)
         REAL,DIMENSION(:,:,:),OPTIONAL ::  EXX,EYY,EZZ,EXY,EXZ,EYZ
        
         REAL,DIMENSION(:,:,:) ::  EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                   ESOUP,ESODW,ELAST
       END SUBROUTINE
       SUBROUTINE POTENTIAL_WZ(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                               ESOUP,ESODW,ELAST,EXX,EYY,&
                               EZZ,EXY,EXZ,EYZ,POT)
         REAL,DIMENSION(:,:,:),OPTIONAL ::  EXX,EYY,EZZ,EXY,EXZ,EYZ,POT
        
         REAL,DIMENSION(:,:,:) ::  EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                   ESOUP,ESODW,ELAST
       END SUBROUTINE
      END INTERFACE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      CALL READ_INPUT()

       RC=Rqd_Base+23.E0
       ZC=Hqd+23.E0


       RD  = Rqd_Base/RC    ! RD = Rqd_Base normalized to RC
       HD  = Hqd/ZC         ! HD = HQD normalized to ZC 

       D=DWL/ZC ! Wetting layer thick. normalized to ZC


!!!  ELECTROSTATIC POTENTIAL AND STRAIN DISTRIBUTION !!!!!!!!!!!!!!!!!!
      
      IF(PZO_Action.EQ.1.AND.STR_Action.EQ.1) THEN

         ALLOCATE (P_SPONT(1:XDim,1:YDim,1:ZDim),&
                   P_PIEZO(1:XDim,1:YDim,1:ZDim) )
         ALLOCATE (EXX(1:XDim,1:YDim,1:ZDim),EYY(1:XDim,1:YDim,1:ZDim), &
                   EZZ(1:XDim,1:YDim,1:ZDim),EXZ(1:XDim,1:YDim,1:ZDim), &  
                   EXY(1:XDim,1:YDim,1:ZDim),EYZ(1:XDim,1:YDim,1:ZDim) )

         CALL CONSTANTS( )

         WRITE(6,*)"Begins the calculation of the Electrostatic Potential"
         CALL PIEZO(P_SPONT,P_PIEZO)
         WRITE(6,*)"Calculation of the Electrostatic Potential ended"
         WRITE(6,*)"Begins the calculation of the Strain Field"
         CALL STRAIN0(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         WRITE(6,*)"Calculation of the Strain Field ended"

         CALL NCPACK_PZ(P_SPONT,P_PIEZO)
         IF (KCOOR.EQ.0) THEN
             CALL NCPACK_CART(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         ELSE
             CALL NCPACK_CYL(EXX,EYY,EZZ,EXZ)
         END IF

      ELSE

        IF(PZO_Action.EQ.1) THEN
         ALLOCATE (P_SPONT(1:XDim,1:YDim,1:ZDim),&
                   P_PIEZO(1:XDim,1:YDim,1:ZDim) )

         CALL CONSTANTS_PZO( )

         WRITE(6,*)"Begins the calculation of the Electrostatic Potential"
         CALL PIEZO(P_SPONT,P_PIEZO)
         WRITE(6,*)"Calculation of the Electrostatic Potential ended"

         CALL NCPACK_PZ(P_SPONT,P_PIEZO)

        END IF
        IF(STR_Action.EQ.1) THEN
         ALLOCATE (EXX(1:XDim,1:YDim,1:ZDim),EYY(1:XDim,1:YDim,1:ZDim), &
                   EZZ(1:XDim,1:YDim,1:ZDim),EXZ(1:XDim,1:YDim,1:ZDim), &  
                   EXY(1:XDim,1:YDim,1:ZDim),EYZ(1:XDim,1:YDim,1:ZDim) )

         CALL CONSTANTS_STR( )

         WRITE(6,*)"Begins the calculation of the Strain Field"
         CALL STRAIN0(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         WRITE(6,*)"Calculation of the Strain Field ended"

         IF (KCOOR.EQ.0) THEN
             CALL NCPACK_CART(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         ELSE
             CALL NCPACK_CYL(EXX,EYY,EZZ,EXZ)
         END IF

        END IF

      END IF

      IF(STR_Action.EQ.2) THEN
         ALLOCATE (EXX(1:XDim,1:YDim,1:ZDim),EYY(1:XDim,1:YDim,1:ZDim), &
                   EZZ(1:XDim,1:YDim,1:ZDim),EXZ(1:XDim,1:YDim,1:ZDim), &  
                   EXY(1:XDim,1:YDim,1:ZDim),EYZ(1:XDim,1:YDim,1:ZDim) )
         IF (KCOOR.EQ.0) THEN
             CALL NCREAD_CART(EXX,EYY,EZZ,EXY,EXZ,EYZ)
         ELSE
             CALL NCREAD_CYL(EXX,EYY,EZZ,EXZ)
         END IF
      END IF

      IF(PZO_Action.EQ.2) THEN
         ALLOCATE (P_SPONT(1:XDim,1:YDim,1:ZDim),&
                   P_PIEZO(1:XDim,1:YDim,1:ZDim) )

         CALL NCREAD_PZ(P_SPONT,P_PIEZO)

      END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  CONFINEMENT POTENTIAL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      IF(POT_Action.NE.0) THEN

         IF (STR_Action.EQ.0 .AND. PZO_Action.EQ.0) CALL CONSTANTS_STR( )

         WRITE(6,*)"Begins the calculation of the Confinement Potential"

         ALLOCATE (EEL(1:XDim,1:YDim,1:ZDim),EHHUP(1:XDim,1:YDim,1:ZDim), &
                   EHHDW(1:XDim,1:YDim,1:ZDim),ELHUP(1:XDim,1:YDim,1:ZDim), &  
                   ELHDW(1:XDim,1:YDim,1:ZDim),ESOUP(1:XDim,1:YDim,1:ZDim), &
                   ESODW(1:XDim,1:YDim,1:ZDim),ELAST(1:XDim,1:YDim,1:ZDim))

        SELECT CASE(MTYPE)

          CASE(1)
            IF(STR_Action.GT.0) THEN
               CALL POTENTIAL_ZB(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                 ESOUP,ESODW,ELAST,EXX,EYY,&
                                 EZZ,EXY,EXZ,EYZ)
            ELSE
               CALL POTENTIAL_ZB(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                 ESOUP,ESODW,ELAST)
            END IF
          CASE(2:)
            IF(STR_Action.NE.0.AND.PZO_Action.NE.0) THEN
                  CALL POTENTIAL_WZ(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                    ESOUP,ESODW,ELAST,EXX,EYY,&
                                    EZZ,EXY,EXZ,EYZ,&
                                    POT=P_SPONT+P_PIEZO)
            ELSE

               IF(STR_Action.NE.0) &
                  CALL POTENTIAL_WZ(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                    ESOUP,ESODW,ELAST,EXX,EYY,&
                                    EZZ,EXY,EXZ,EYZ)

               IF(PZO_Action.NE.0) &
                  CALL POTENTIAL_WZ(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                    ESOUP,ESODW,ELAST,&
                                    POT=P_SPONT+P_PIEZO)
            END IF
            IF(STR_Action.EQ.0.AND.PZO_Action.EQ.0) THEN
                  CALL POTENTIAL_WZ(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                                    ESOUP,ESODW,ELAST)
            END IF
        
        END SELECT

         WRITE(6,*)"Calculation of the Confinement Potential ended"

            CALL NCPACK_PT(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                           ESOUP,ESODW,ELAST)
      END IF
       
      IF(DPL_Action.EQ.1) THEN
       
         IF(.NOT.(STR_Action.EQ.1.AND.PZO_Action.EQ.1)) &
                  CALL CONSTANTS_DSP( )
        
         ALLOCATE (UX(1:XDim,1:YDim,1:ZDim),UY(1:XDim,1:YDim,1:ZDim), &
                   UZ(1:XDim,1:YDim,1:ZDim), UR(1:XDim,1:YDim,1:ZDim) )

         WRITE(6,*)"Begins the calculation of the Displacement Field"
         CALL DISPLACEMENT(UR,UX,UY,UZ)
         WRITE(6,*)"Calculation of the Displacement Field ended"

         IF (KCOOR.EQ.0) THEN
             CALL NCPACK_DPL_CART(UX,UY,UZ)
         ELSE
             CALL NCPACK_DPL_CYL(UR,UZ)
         END IF
                        

      END IF

  END PROGRAM POTDRV
