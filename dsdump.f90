
  PROGRAM EXTRACT_VALUES

  USE NETCDF

 IMPLICIT NONE

 REAL, DIMENSION(:,:), ALLOCATABLE :: UR,UZ,EZZ,ERR,ERZ,E00, EHID
 REAL  CORTE, ZMIN, ZMAX, ZINC, ZETA, EX, EZ, EX_M, EX_V, &
       EZ_M, EZ_V, RMIN, RMAX, RHO, RINC,DWL,E15,E31,E33
 REAL, DIMENSION(:), ALLOCATABLE :: DX,DY 
 INTEGER  FILE_ID, D2, DIMX, DIMY, I, J, AUX, err_ID, erz_ID, ezz_ID, &
          numpar,II, ehid_ID, I_CONT, DX_ID, DY_ID,e00_ID,ur_ID,uz_ID,&
          FILEU_ID
 CHARACTER(LEN=80) :: File_OUT
 
 call check(nf90_open("STR_QD.nc", nf90_NOWRITE,FILE_ID))
 call check(nf90_open("DPL_QD.nc", nf90_NOWRITE,FILEU_ID))

 call check(nf90_Inquire_Dimension(FILE_ID,2,len=DIMX))
 call check(nf90_Inquire_Dimension(FILE_ID,3,len=DIMY))


 ALLOCATE (EZZ(DIMX,DIMY),ERR(DIMX,DIMY),E00(DIMX,DIMY),   &
           EHID(DIMX,DIMY), ERZ(DIMX,DIMY),DX(DIMX),DY(DIMY), &
           UR(DIMX,DIMY),UZ(DIMX,DIMY))

  call check(nf90_inq_varid(FILE_ID, "err", err_ID))
  call check(nf90_inq_varid(FILE_ID, "e00", e00_ID))
  call check(nf90_inq_varid(FILE_ID, "erz", erz_ID))
  call check(nf90_inq_varid(FILE_ID, "ezz", ezz_ID))
  call check(nf90_inq_varid(FILEU_ID, "uz", uz_ID))
  call check(nf90_inq_varid(FILEU_ID, "ur", ur_ID))
  call check(nf90_inq_varid(FILE_ID, "dimx", DX_ID))
  call check(nf90_inq_varid(FILE_ID, "dimz", DY_ID))
  call check(nf90_get_var(FILE_ID, erz_ID, ERZ))
  call check(nf90_get_var(FILE_ID, ezz_ID, EZZ))
  call check(nf90_get_var(FILE_ID, err_ID, ERR))
  call check(nf90_get_var(FILE_ID, e00_ID, E00))
  call check(nf90_get_var(FILEU_ID, ur_ID, UR))
  call check(nf90_get_var(FILEU_ID, uz_ID, UZ))
  call check(nf90_get_var(FILE_ID, DX_ID, DX))
  call check(nf90_get_var(FILE_ID, DY_ID, DY))

  CORTE=-0.01

  FILE_OUT="DPL_STR.dat"
  OPEN(20,FILE=TRIM(FILE_OUT))
   DO J=1,DIMY
    ZETA =DY(J)
  DO I=1,DIMX
   RHO=DX(I)
! ONLY inside the WL
!   IF(ZETA.LE.0.E0.AND.ZETA.GE.-DWL) THEN
! ONLY inside the DOT
!   IF(ZETA.GT.0.E0            .AND. &
!      ZETA.LT.(HD(II)+5.)     .AND. &
!      ABS(RHO).LT.(RB(II)+5.0)      ) THEN
! ONLY above the WL
!  IF(ZETA.GT.-DWL+0.1.AND.ZETA.LT.HD(II)+0.1) THEN
! ONLY inside the DOT + WL
!   IF(ZETA.GT.(-DWL-2.)          .AND. &
!      ZETA.LT.(HD(II)+5.) ) THEN

!    IF(EHID(i,j,II).GT.CORTE) THEN
        WRITE(20,'(20(E15.8,1X))')RHO,ZETA,UR(I,J),UZ(I,J),ERR(I,J),E00(I,J),EZZ(I,J),ERZ(I,J)
!    ELSE
!       WRITE(20,'(3(E15.8,1X))')RHO,ZETA,1.E0
!       WRITE(30,'(3(E15.8,1X))')RHO,ZETA,1.E0
!    END IF

!  END IF
   END DO
!  To plot with PM3D
   WRITE(20,*)""
  END DO
  WRITE(6,*)"OUTPUT WRITED TO "//TRIM(FILE_OUT)
  close(20)


 DEALLOCATE(EZZ,ERR,E00,ERZ,UR,UZ,DX,DY)

 call check(nf90_close(FILE_ID))
 call check(nf90_close(FILEU_ID))

 CONTAINS

  subroutine check(status)
    integer, intent ( in) :: status
    
    if(status /= nf90_noerr) then 
      print *, trim(nf90_strerror(status))
    end if
  end subroutine check  

END PROGRAM EXTRACT_VALUES
