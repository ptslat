!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!      This subroutine calculates the strain distribution              !
!              inside and around the quantum dot                       !
!                                                                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SUBROUTINE PIEZO(P_SPONT,P_PIEZO)

              Use Input_Data
              Use Dot_Geometry
              Use Auxiliar_Procedures, ONLY : AISO
              Use PZO_CALC
      
      IMPLICIT NONE

!!!!! 'dummy' and local variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

      REAL         ZM,THETA,CTHETA,STHETA,CHI, &
                   PZ_1,PZ_ETA1,PZ_ETA2,&
                   P_SP, P_PZ, X,Y,Z,ZMAUX,&
                   WL_AUX,BR_AUX,POTSP,POTPZ

      REAL,DIMENSION(:,:,:) ::  P_SPONT,P_PIEZO 
      INTEGER      I_X,I_Y,I_Z,I_N1,I_N2,I_N3

      REAL, DIMENSION(3) :: R_SL,X_VEC,XI_VEC

      !!! COMMON
      REAL :: RHO,ZETA,ETA
      COMMON /QAGON/RHO,ZETA,ETA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      P_SPONT=0.E0;P_PIEZO=0.E0

ZD:   DO I_Z=1,ZDim
!       WRITE(16,'(A,I3,A,I3)')"I_Z ",I_Z," of ",ZDIM
        Z=Z_Min+REAL(I_Z-1)*Z_Inc
YD:   DO I_Y=1,YDim
        Y=Y_Min+REAL(I_Y-1)*Y_Inc
XD:   DO I_X=1,XDim
        X=X_Min+REAL(I_X-1)*X_Inc

        X_VEC=(/X,Y,Z/)

        P_SP=0.E0;P_PZ=0.E0 

N3:   DO I_N3=NMin_Z,NMax_Z
N2:   DO I_N2=NMin_Y,NMax_Y
N1:   DO I_N1=NMin_X,NMax_X

      R_SL=REAL(I_N1)*A1_S+REAL(I_N2)*A2_S+REAL(I_N3)*A3_S

      XI_VEC=X_VEC-R_SL

      RHO=SQRT(XI_VEC(1)**2+XI_VEC(2)**2)/RC
      IF(XI_VEC(1).EQ.0.E0.AND.XI_VEC(2).EQ.0.E0) THEN
              CTHETA=1.E0/SQRT(2.E0); STHETA=1.E0/SQRT(2.E0) ! It is not the Mathematical limit
      ELSE
              THETA=ATAN(XI_VEC(2)/XI_VEC(1))
              CTHETA=Cos(THETA); STHETA=Sin(THETA)
      END IF
      ZETA=XI_VEC(3)/ZC

          IF (RHO.LE.RD) THEN
             CALL SHAPERTOZ(MIN(RHO*RC,Rqd_Base),ZMAUX)
             ZM=ZMAUX/ZC
          ELSE
             ZM = 0.E0
          END IF

    
          IF (abs(zeta) .EQ. 0.E0 .OR. ZETA .EQ. ZM) THEN 
              ZETA=ZETA-1.E-5
          END IF

          CHI = 0.
          IF (RHO.LE.RD.AND.ZETA.GE.0.E0.AND.ZETA.LE.ZM) THEN
                  CHI = 1.
                  IF(I_N1.NE.0.OR.I_N2.NE.0.OR.I_N3.NE.0) THEN
                          WRITE(16,*)I_N1,I_N2,I_N3
                          WRITE(16,*)X_VEC(3),ZETA*ZC,ZM*ZC
                  END IF
          END IF

          IF (HD.NE.0.E0) THEN
              IF(AISO.EQ.1) THEN
                 CALL PZOISO(POTSP,POTPZ)
              ELSE
                 CALL PZOANISO(POTSP,POTPZ)
              END IF
          ELSE
            POTSP=0.E0; POTPZ=0.E0
          END IF

          P_SP=P_SP+POTSP
          P_PZ=P_PZ+POTPZ

!!!!!!!!!!!!!!!!!! Wetting Layer !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          IF(I_N1.EQ.0.AND.I_N2.EQ.0.AND.I_N3.EQ.0) THEN

               WL_aux = ( abs(ZETA-(-D)) - abs(ZETA) )*ZC/RC
               BR_aux = ( abs(ZETA-(-C_S/ZC)) - abs(ZETA-(-D)) )*ZC/RC +&
                        ( abs(ZETA) - abs(ZETA-(C_S/ZC-D)) )*ZC/RC

            P_SP=P_SP+CPZ/2.E0*RC*(CSPWL*WL_aux+CSPBR*BR_Aux)
            P_PZ=P_PZ+CPZ/2.E0*RC*CPZWL*WL_AUX

           END IF
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      END DO N1
      END DO N2
      END DO N3

           P_SPONT(I_X,I_Y,I_Z)= P_SP
           P_PIEZO(I_X,I_Y,I_Z)= P_PZ
         
!           WRITE(26,'(10(E15.8,1X))')Z,P_SP,P_PZ,P_SP+P_PZ
       
      END DO XD
      END DO YD
      END DO ZD

!     STOP

      RETURN
      END SUBROUTINE PIEZO

      DOUBLE PRECISION FUNCTION I_FI00(X)
       Use PZO_CALC
       Use Dot_Geometry
       Use Input_Data, ONLY: RC,ZC
       IMPLICIT NONE
      
       DOUBLE PRECISION :: X
       !!! COMMON
       REAL :: RHO,ZETA,ETA,ZM
       COMMON /QAGON/RHO,ZETA,ETA

       CALL SHAPERTOZ(MIN(SNGL(X)*RC,Rqd_Base),ZM)
       I_FI00=X*FI00(DBLE(RHO),DBLE(ZETA),X,DBLE(ZM/ZC),DBLE(ETA))

       RETURN

      END FUNCTION I_FI00

      DOUBLE PRECISION FUNCTION I_FDA00(X)
       Use PZO_CALC
       Use Dot_Geometry
       Use Input_Data, ONLY: RC,ZC
       IMPLICIT NONE
      
       DOUBLE PRECISION :: X
       !!! COMMON
       REAL :: RHO,ZETA,ETA,ZM
       COMMON /QAGON/RHO,ZETA,ETA

       CALL SHAPERTOZ(MIN(SNGL(X)*RC,Rqd_Base),ZM)
       I_FDA00=X*FDA00(DBLE(RHO),DBLE(ZETA),X,DBLE(ZM/ZC),DBLE(ETA))

       RETURN

      END FUNCTION I_FDA00

