MODULE NCPACK_POT
         Use typeSizes
         Use NETCDF
         Use Dot_Geometry, ONLY : Rqd_Base,Rqd_Top,Hqd,ISHAPE
         Use Input_Data
         Use Auxiliar_Procedures, ONLY : MTYPE, check, open_over
  IMPLICIT NONE

  INTEGER,PRIVATE :: ncFileID, HdVarID, RtVarID, RbVarID, NDots_XVarID, &
                     NDots_YVarID,NDots_ZVarID, A1VarID, A2VarID, A3VarID, &
                     DWL_VarID, Dim3ID

CONTAINS

  SUBROUTINE NCPACK_PT(EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                       ESOUP,ESODW,ELAST)
    IMPLICIT NONE
    
    REAL,DIMENSION(:,:,:) ::  EEL,EHHUP,EHHDW,ELHUP,ELHDW,&
                              ESOUP,ESODW,ELAST
 
    ! netcdf related variables
    integer :: DimXID, DimYID, DimZID,i,  &
               DimXVarID, DimYVarID, DimZVarID,&
               eelVarID,ehhupVarID,ehhdwVarID,elhupVarID,&
               elhdwVarID,esoupvarID,esodwvarID,elastvarID
                              
    ! Local variables
    character (len = 120) :: fileName
 
    real, dimension(1:XDim,1:ZDim) :: POT_AUX

    !-----
    ! Initialization
    !-----
 
    fileName = POT_Filename
 
 
    ! --------------------
    ! Code begins
    ! --------------------
    if(.not. byteSizesOK()) then
      print *, "Compiler does not appear to support required kinds of variables."
      stop
    end if
      
    ! Create the file
 
    call open_over(fileName) !Check if a file with the same name exists
    
    call check(nf90_create(path = trim(fileName), cmode = nf90_clobber, ncid = ncFileID))
    
    CALL NCPACK_INPUT_POT( )
    
    ! Define the dimensions
    call check(nf90_def_dim(ncid = ncFileID, name = "dimx",     len = XDim,        dimid = DimXID))
    IF(KCOOR.EQ.0) &
    call check(nf90_def_dim(ncid = ncFileID, name = "dimy",     len = YDim,        dimid = DimYID))
    call check(nf90_def_dim(ncid = ncFileID, name = "dimz",     len = ZDim,        dimid = DimZID))
 
    ! Create variables and attributes
 
    ! Box sizes
 
 
    call check(nf90_def_var(ncFileID, "dimx", nf90_float, dimids = DimXID, varID = DimXVarID) )
    call check(nf90_put_att(ncFileID, DimXVarID, "long_name", "Radial dimension"))
    call check(nf90_put_att(ncFileID, DimXVarID, "units",     "Angstroms"))
 
    IF (KCOOR.EQ.0) THEN
    call check(nf90_def_var(ncFileID, "dimy", nf90_float, dimids = DimYID, varID = DimYVarID) )
    call check(nf90_put_att(ncFileID, DimYVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimYVarID, "units",     "Angstroms"))
    END IF
 
    call check(nf90_def_var(ncFileID, "dimz", nf90_float, dimids = DimZID, varID = DimZVarID) )
    call check(nf90_put_att(ncFileID, DimZVarID, "long_name", "Z dimension"))
    call check(nf90_put_att(ncFileID, DimZVarID, "units",     "Angstroms"))
 
 
    ! potential grids
    SELECT CASE(KCOOR)
    CASE(0)
       call check(nf90_def_var(ncid = ncFileID, name = "EEL", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = eelVarID) )
       call check(nf90_put_att(ncFileID, eelVarID, "long_name", "Electron Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "EHHUP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = ehhupVarID) )
       call check(nf90_put_att(ncFileID, ehhupVarID, "long_name",  "Heavy Hole UP Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "EHHDW", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = ehhdwVarID) )
       call check(nf90_put_att(ncFileID, ehhdwVarID, "long_name",   "Heavy Hole DW Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ELHUP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = elhupVarID) )
       call check(nf90_put_att(ncFileID, elhupVarID, "long_name",   "Light Hole UP Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ELHDW", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = elhdwVarID) )
       call check(nf90_put_att(ncFileID, elhdwVarID, "long_name",   "Light Hole DW Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ESOUP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = esoupVarID) )
       call check(nf90_put_att(ncFileID, esoupVarID, "long_name",   "Split-off UP Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ESODW", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = esodwVarID) )
       call check(nf90_put_att(ncFileID, esodwVarID, "long_name",   "Split-off DW Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ELAST", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimYID, DimZID /), varID = elastVarID) )
       call check(nf90_put_att(ncFileID, elastVarID, "long_name",   "Strain Energy"))
    CASE(1)
       call check(nf90_def_var(ncid = ncFileID, name = "EEL", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = eelVarID) )
       call check(nf90_put_att(ncFileID, eelVarID, "long_name", "Electron Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "EHHUP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = ehhupVarID) )
       call check(nf90_put_att(ncFileID, ehhupVarID, "long_name",  "Heavy Hole UP Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "EHHDW", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = ehhdwVarID) )
       call check(nf90_put_att(ncFileID, ehhdwVarID, "long_name",   "Heavy Hole DW Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ELHUP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = elhupVarID) )
       call check(nf90_put_att(ncFileID, elhupVarID, "long_name",   "Light Hole UP Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ELHDW", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = elhdwVarID) )
       call check(nf90_put_att(ncFileID, elhdwVarID, "long_name",   "Light Hole DW Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ESOUP", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = esoupVarID) )
       call check(nf90_put_att(ncFileID, esoupVarID, "long_name",   "Split-off UP Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ESODW", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = esodwVarID) )
       call check(nf90_put_att(ncFileID, esodwVarID, "long_name",   "Split-off DW Potential"))
    
       call check(nf90_def_var(ncid = ncFileID, name = "ELAST", xtype = nf90_float,     &
                          dimids = (/ DimXID, DimZID /), varID = elastVarID) )
       call check(nf90_put_att(ncFileID, elastVarID, "long_name",   "Strain Energy"))
    END SELECT 
    ! In the C++ interface the define a scalar variable - do we know how to do this? 
    !call check(nf90_def_var(ncFileID, "ScalarVariable", nf90_real, scalarVarID))
    
    ! Leave define mode
    call check(nf90_enddef(ncfileID))
    
    ! Write the dimension variables
 
    call check(nf90_put_var(ncFileID, DimXVarId,     (/(X_Min+REAL(I-1)*X_Inc, i=1,XDim)/)) )
    IF(KCOOR.EQ.0) &
    call check(nf90_put_var(ncFileID, DimYVarId,     (/(Y_Min+REAL(I-1)*Y_Inc, i=1,YDim)/)) )
    call check(nf90_put_var(ncFileID, DimZVarId,     (/(Z_Min+REAL(I-1)*Z_Inc, i=1,ZDim)/)) )
 
 
    ! Write the Strain distributions
    
    SELECT CASE(KCOOR)
    CASE(0)
      call check(nf90_put_var(ncFileID, eelVarID, EEL) )
      call check(nf90_put_var(ncFileID, ehhupVarID, EHHUP) )
      call check(nf90_put_var(ncFileID, ehhdwVarID, EHHDW) )
      call check(nf90_put_var(ncFileID, elhupVarID, ELHUP) )
      call check(nf90_put_var(ncFileID, elhdwVarID, ELHDW) )
      call check(nf90_put_var(ncFileID, esoupVarID, ESOUP) )
      call check(nf90_put_var(ncFileID, esodwVarID, ESODW) )
      call check(nf90_put_var(ncFileID, elastVarID, ELAST) )
    CASE(1)
      POT_AUX=EEL(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, eelVarID, POT_AUX) )
      POT_AUX=EHHUP(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, ehhupVarID, POT_AUX) )
      POT_AUX=EHHDW(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, ehhdwVarID, POT_AUX) )
      POT_AUX=ELHUP(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, elhupVarID, POT_AUX) )
      POT_AUX=ELHDW(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, elhdwVarID, POT_AUX) )
      POT_AUX=ESOUP(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, esoupVarID, POT_AUX) )
      POT_AUX=ESODW(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, esodwVarID, POT_AUX) )
      POT_AUX=ELAST(1:XDim,1,1:ZDim)
       call check(nf90_put_var(ncFileID, elastVarID, POT_AUX) )
    END SELECT
 
    call check(nf90_close(ncFileID))
 
  END SUBROUTINE NCPACK_PT

  SUBROUTINE NCPACK_INPUT_POT( )
    IMPLICIT NONE

    CHARACTER(LEN=200) :: stitle

    call check(nf90_def_dim(ncid = ncFileID, name = "vector",   len = 3,           dimid = Dim3ID))

    call check(nf90_def_var(ncid = ncFileID, name = "HD", xtype = nf90_float,     &
                            varID = HdVarID) )
    call check(nf90_put_att(ncFileID, HdVarID, "long_name",   "Dot Height (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RT", xtype = nf90_float,     &
                            varID = RtVarID) )
    call check(nf90_put_att(ncFileID, RtVarID, "long_name",   "Top Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "RB", xtype = nf90_float,     &
                            varID = RbVarID ) )
    call check(nf90_put_att(ncFileID, RbVarID, "long_name",   "Base Dot Radius (Angstroms)"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_X", xtype = nf90_int,     &
                            varID = NDots_XVarID ) )
    call check(nf90_put_att(ncFileID, NDots_XVarID, "long_name",   "Number of Dots along X-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Y", xtype = nf90_int,     &
                            varID = NDots_YVarID ) )
    call check(nf90_put_att(ncFileID, NDots_YVarID, "long_name",   "Number of Dots along Y-axis"))
 
    call check(nf90_def_var(ncid = ncFileID, name = "ND_Z", xtype = nf90_int,     &
                            varID = NDots_ZVarID ) )
    call check(nf90_put_att(ncFileID, NDots_ZVarID, "long_name",   "Number of Dots along Z-axis"))

    call check(nf90_def_var(ncFileID, "A1", nf90_float, dimids = Dim3ID, varID = A1VarID) )
    call check(nf90_put_att(ncFileID, A1VarID, "long_name", "Vector A1 Superlattice"))
    call check(nf90_put_att(ncFileID, A1VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A2", nf90_float, dimids = Dim3ID, varID = A2VarID) )
    call check(nf90_put_att(ncFileID, A2VarID, "long_name", "Vector A2 Superlattice"))
    call check(nf90_put_att(ncFileID, A2VarID, "units",     "Angstroms"))
 
    call check(nf90_def_var(ncFileID, "A3", nf90_float, dimids = Dim3ID, varID = A3VarID) )
    call check(nf90_put_att(ncFileID, A3VarID, "long_name", "Vector A3 Superlattice"))
    call check(nf90_put_att(ncFileID, A3VarID, "units",     "Angstroms"))

    call check(nf90_def_var(ncFileID, "DWL", nf90_float, varID = DWL_VarID))

    SELECT CASE(ISHAPE)
      CASE(1)
        stitle="Shape: Truncated Cone. "
      CASE(2)
        stitle="Shape: Cap. "
    END SELECT

    SELECT CASE(MTYPE)
      CASE DEFAULT !! Anisotropic Wurtzite
        stitle="Material Type: Anisotropic Wurtzite. "//stitle
    END SELECT

    ! Global attributes
    call check(nf90_put_att(ncFileID, nf90_global, "history", &
                       "NetCDF file"))
    stitle="Confinement Potential. "//stitle
    
    call check(nf90_put_att(ncFileID, nf90_global, "title", TRIM(stitle)))
    
    call check(nf90_enddef(ncfileID))

    call check(nf90_put_var(ncFileID, RbVarId, Rqd_Base ) )
    call check(nf90_put_var(ncFileID, RtVarId, Rqd_Top ) )
    call check(nf90_put_var(ncFileID, HdVarId, Hqd ) )
    call check(nf90_put_var(ncFileID, DWL_VarId, D*ZC ) )
    call check(nf90_put_var(ncFileID, NDots_XVarId, NMax_X-NMin_X+1 ) )
    call check(nf90_put_var(ncFileID, NDots_YVarId, NMax_Y-NMin_Y+1 ) )
    call check(nf90_put_var(ncFileID, NDots_ZVarId, NMax_Z-NMin_Z+1 ) )
    call check(nf90_put_var(ncFileID, A1VarId, A1_S ) )
    call check(nf90_put_var(ncFileID, A2VarId, A2_S ) )
    call check(nf90_put_var(ncFileID, A3VarId, A3_S ) )


    call check(nf90_redef(ncfileID))

    RETURN

  END SUBROUTINE NCPACK_INPUT_POT

END MODULE NCPACK_POT

