MODULE ELIPT_MOD

  IMPLICIT NONE

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                Heuman's function  (KP!!2 = 1-K!!2)                   !
!                                                                      !
!                     (PI/2)* LAMBDA0(PSI,K) =                         !
!                                                                      !
!          = E(K)*F(PSI,KP) + K(K)*( E(PSI,KP) - F(PSI,KP) )           ! 
!                                                                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION FUNCTION LAMBDA0(PSI,K)
 
!!!!! variables 'dummy' y variables internas !!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION &
                   PSI,K,KFUN,EFUN,KP,FINTEL,EINTEL,PI,DPI

!!!!! COMMON variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 

      PI  = DATAN(1.D0)*4.D0 
      DPI = 2.D0*PI       

      CALL ELIPT0(K,KFUN,EFUN)

      KP = DSQRT(1.D0-K**2) 
 
      CALL ELIPT(PSI,KP,FINTEL,EINTEL)
 
      LAMBDA0 = (2.D0/PI)*( EFUN*FINTEL + KFUN*( EINTEL-FINTEL ) )

      RETURN
      END FUNCTION LAMBDA0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Incomplete elliptic integrals
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE ELIPT(PSI,K,FINTEL,EINTEL)

!!!!! variables 'dummy' y variables internas !!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION  PSI,K,FINTEL,EINTEL,S,C
      INTEGER      IERR 

!!!!! function names !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION DRF,DRD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      S = DSIN(PSI)
      C = DCOS(PSI) 

      FINTEL = S*DRF(C**2,1.D0-(K*S)**2,1.D0,IERR)     

      IF (IERR.NE.0) THEN
         WRITE(6,*) 'STOP EN FINTEL','IERR =',IERR
         STOP
      END IF

      EINTEL = FINTEL  &
             - (K**2/3.D0)*S**3*DRD(C**2,1.D0-(K*S)**2,1.D0,IERR)

      IF (IERR.NE.0) THEN
         WRITE(6,*) 'STOP EN EINTEL','IERR =',IERR
         STOP
      END IF

      RETURN
      END SUBROUTINE ELIPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Complete elliptic integrals
!     poner los limites en K = 0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE ELIPT0(K,KFUN,EFUN)

!!!!! variables 'dummy' y variables internas !!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION K,KFUN,EFUN
      INTEGER      IERR 

!!!!! function names !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION DRF,DRD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      KFUN = DRF(0.D0,1.D0-K**2,1.D0,IERR)

      IF (IERR.NE.0) THEN
         WRITE(6,*) 'STOP IN KFUN','IERR =',IERR
         STOP
      END IF

      EFUN = KFUN  &
           - (K**2/3.D0)*DRD(0.D0,1.D0-K**2,1.D0,IERR)

      IF (IERR.NE.0) THEN
         WRITE(6,*) 'STOP IN EFUN','IERR =',IERR
         STOP
      END IF

      RETURN
      END SUBROUTINE ELIPT0

      SUBROUTINE ELIPTK0(K,KFUN)

!!!!! variables 'dummy' y variables internas !!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION K,KFUN
      INTEGER      IERR 

!!!!! function names !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      DOUBLE PRECISION DRF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      KFUN = DRF(0.D0,1.D0-K**2,1.D0,IERR)

      IF (IERR.NE.0) THEN
         WRITE(6,*) 'STOP IN KFUN','IERR =',IERR
         STOP
      END IF

      RETURN
      END SUBROUTINE ELIPTK0

END MODULE ELIPT_MOD
